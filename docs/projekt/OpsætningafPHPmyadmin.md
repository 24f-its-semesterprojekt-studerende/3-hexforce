### Krav
- En Ubuntu server. (Ser det helst som 18.04, dog burde det være muligt på enhver version, så længe at den installerede PHP version ikke er højere end 7.4)

### Opsætning
Det første der var behov for, var installationen af en apache webserver, som ville gøre det muligt at tilgå PHPmyadmin senere. Dette gøres ved:

- ```-	sudo apt install apache2```

Her efter installeres:

- ```-	sudo apt install mysql-server```
    - God ide at sætte kodeord nu.
- ```-	sudo apt install PHP```
    - Tjek versionen før installering.
    - ![alt text](../projekt/projektbilleder/TjekPHPversion.png)
- ```-	sudo apt install libapache2-mod-php```
- ```-	sudo apt install php-mysql```

### PHPMyAdmin
Nu kan PHPMyAdmin installeres. Dette gøres manuelt for at få fat i versionen der er er udsat for CVE-2018-12613.
Først skal tar filen hentes ned:

-	```sudo wget https://files.phpmyadmin.net/phpMyAdmin/4.8.1/phpMyAdmin-4.8.1-all-languages.tar.gz```

Og derefter udpakkes:

- ```sudo tar -xf phpMyAdmin-4.8.1-all-languages.tar.gz```

Flyt den til /usr/share/

- ```sudo mv phpMyAdmin-4.8.1-all-languages /usr/share/```

Omdøb mappen til "phpmyadmin"

- ```sudo mv /usr/share/phpMyAdmin-4.8.1-all-languages /usr/share/phpmyadmin```

Herefter skal brugeren "www-data" have ejerskab over mappen.

- ```sudo chown -R www-data /usr/share/phpmyadmin```

Og da vi senere skal rykke rundt på nogle filer, giver vi også alle read og write rettigheder:

- ```sudo chmod -R 755 /usr/share/phpMyAdmin```

Kopier den sample konfigurations fil PHPMyAdmin kommer med, så den bruges af PHPMyAdmin:

- ```sudo cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php```

### Apache
Lav en fil kaldet "phpmyadmin.conf" i /etc/apache2/conf-available  
Denne fil skal se sådan her ud:  
![alt text](../projekt/projektbilleder/phpmyadminconf.png)

Herefter, tilføj linen "extension=mysqli.so" til filen "/etc/php/7.2/apache2/php.ini"  

- Dette kommer an på versionen af PHP, erstat 7.2 med hvad end versionen installeret er.

Sæt Apache til at bruge den tidligere lavet konfigurations fil:  

- ```sudo a2enconf phpmyadmin```

Og genstart Apache servicen:  

- ```sudo systemctl restart apache2```

### Hjemmesiden
Du burde nu kunne tilgå hjemmesiden:  
![alt text](../projekt/projektbilleder/PHPlandingpage.png)