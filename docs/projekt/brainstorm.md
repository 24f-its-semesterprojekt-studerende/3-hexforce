# Brainstorm for projekt
## Overordnet brainstorm:
- Firewall
- Logning programmer:
    - Graylog, Wazuh
- IDS/IPS
    - Surikata
        - Hvilke regler?
        - egen regel
- Segementering af netværk
    - DMZ, "almindelig brugere" på netværket, afgrænsninger
        - Brugere med forskellige rettigheder 
        - Opsætningen af passwords der overholder et password politik
        
Opsætning af VPLE for at beskytte andre enheder.
- Vise angrebsmeotder etc.
FUZZing mod webservere indenfor netværket
- Brute force mod VPLE
    - Dårlig adgangskoder

Benchmarks 
- CIS18

Opfange ondsindet netværkstrafik med Wazuh/Graylog
- Brugen af de logs til at opfinde en modforanstaltning
IPAM (netbox), til at holde overblik over hele netværket og de dele der i den.

## liste over emner indenfor netværk og system sikkerhed:

- Viden
    - Netværkstrusler
    - ~~Trådløs sikkerhed~~
    - Sikkerhed i TCP/IP
    - Adressering i de forskellige lag
    - Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)
    - Hvilke enheder, der anvender hvilke protokoller
    - Forskellige sniffing strategier og teknikker
    - Netværk management (overvågning/logning, snmp)
    - ~~Forskellige VPN setups~~
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
- færdiheder
    - Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
        - Logning
    - Teste netværk for angreb rettet mod de mest anvendte protokoller
        - Teste diverse angrebsmetoder af, og anvende foranstaltninger til at opdage/forhindre dem
            - MiTRE
    - Identificere sårbarheder som et netværk kan have.
- kompetencer
    - Designe, konstruere og implementere samt teste et sikkert netværk
        - Segementering, compartmentilization
    - Monitorere og administrere et netværks komponenter
        - Logning over forskellige enheder.
    - Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)
    - Opsætte og konfigurere et IDS eller IPS
        - Surikata på pfsense(?)

## System sikkerhed:
- viden
    - Generelle governance principper / sikkerhedsprocedurer
    - Væsentlige forensic processer
        - Wazuh, Graylog
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
        - Brug MiTRE til at finde angrebsvektorer og metoder
    - OS roller ift. sikkerhedsovervejelser
        - "Bruger" netværket
    - Sikkerhedsadministration i DBMS.
- Færdigheder
    - Udnytte modforanstaltninger til sikring af systemer
        - Brug logs/forensic til at finde og løse huller
    - Følge et benchmark til at sikre opsætning af enhederne
        - CIS 18
    - Implementere systematisk logning og monitering af enheder
    - Analysere logs for incidents og følge et revisionsspor
    - Kan genoprette systemer efter en hændelse.
        - Lave snapshots eller templates.
        - 3-2-1
- Kompetancer
    - håndtere enheder på command line-niveau
    - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
    - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
    - håndtere relevante krypteringstiltag





- Purple team excersize
- Defense til at starte med
- Perespektiv med en virksomhed.
  

## Problem formulering: 
Hovedproblem: Hvordan kan man sikre en eller flere usikker applikation(er)/enhed(er), som der er behov for i en virksomhed?
- Hvilke tiltag er nødvendige for at opnå et tilstrækkeligt sikkerhedsniveau?
  - Definer et tilstrækkeligt sikkerhedsniveau.
- Hvilke trussler er der overfor applikationen?
  - Hvilke angrebsmetoder kan der bruges mod applikationen(er)/enhed(er)?
- Hvilke metoder kan bruges til at øge sikkerheden i et system?

### afgrænsning
Andre usikker applikationer:
- Webgoat.net 
- Find en CVE
    - installer den uskire applikation på nginxVM 
- Forskellige måder at tilgå maskinen (SSH, etc.)
- Proxmox skal kun bruges til opsætningen af infrastruktur, alt konfiguration skal gøres gennem en forbindelse, hvor det er muligt.
    - Dette er for at simulerer en virksomhed der har fysiske maskiner.
- Rapporten laves ud fra synspunkt som konsulenter(?)


Firewall regler:
- DMZ
  - DMZ skal kunne tilgås udefra.
    - Adgang til/fra 00WAN
      - Specifikke porte?
- MANAGEMENT
  - Ingen ændringer
- MONITOR
  - Wazuh agent på VPLE maskinen
- USERS
  - Adgang til 00WAN
  - Blocklist eller allowlist
  - Hvilke slags brugere simulerer vi
  - 


  cis 18 alt efter hvor stor virksomheden er.
  Kig på noget exploit kode, fra en specifik CVE.
    - kig på mitre mittigeringer.
  Hvilke foranstaltninger kan vi lave på netværket?
  Hvilke foranstaltninger kan vi lave på host?

