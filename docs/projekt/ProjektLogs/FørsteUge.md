---
 hide:
#  - footer
---

# Læringslog - første uge

## Mål for ugen

- Få udarbejdet en problemformulering.
- Få opsat nyt netværk til brugerne.
- Opsætningen af phpMyAdmin
- Logning

### Praktiske mål
**Praktiske opgaver vi har udført**

- [phpMyAdmin](../OpsætningafPHPmyadmin.md)
- Opsat logning for mySQL (dette endte med ikke at blive brugt til noget)

### Læringsmål

**Læringsmål vi har arbejdet med**

Systemsikkerhed:

- **Viden:** 
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
    - Implementere systematisk logning og monitering af enheder
- **Kompetencer:**
    - håndtere enheder på command line-niveau

Netværkssikkerhed:

- **Viden:**
    - Netværkstrusler
    - Netværk management
    - Gængse netværksenheder der bruges ifm. sikkerhed
- **Færdigheder:**
    - Teste netværk for angreb rettet mod de mest anvendte protokoller
    - Identificere sårbarheder som et netværk kan have.
- **Kompetencer:**
    - Designe, konstruere og implementere samt teste et sikkert netværk
    - Monitorere og administrere et netværks komponenter

## Reflektioner over hvad vi har lært

- Vi mener vi er godt på vej. Vi har fået vores CVE op og køre, og er klar til at implementer foranstaltninger.

## Andet