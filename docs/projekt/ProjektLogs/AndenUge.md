---
 hide:
#  - footer
---

# Læringslog - anden uge

## Mål for ugen

- Få opsat IDS/IPS
- Skrevet i rapporten.
- Implementering af benchmarks.
- Logging relaterede ting.
- Firewalls

### Praktiske mål

**Praktiske opgaver vi har udført**

- [Gennemgang af CVE angreb](../ExploitGennemgang.md)
- [IDS/IPS opsætning](../SuricataReglerGennemgang.md)
- [Firewall Regler](../firewall_rules.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

Systemsikkerhed:

- **Viden:**
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
    - Udnytte modforanstaltninger til sikring af systemer
    - Implementere systematisk logning og monitering af enheder
    - Analysere logs for incidents og følge et revisionsspor
- **Kompetencer:**
    - håndtere enheder på command line-niveau
    - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
    - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.

Netværkssikkerhed:

- **Viden:**
    - Netværkstrusler
    - Sikkerhed i TCP/IP
    - Forskellige sniffing strategier og teknikker
- **Færdigheder:**
    - Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
    - Teste netværk for angreb rettet mod de mest anvendte protokoller
    - Identificere sårbarheder som et netværk kan have.
- **Kompetencer:**
    - Designe, konstruere og implementere samt teste et sikkert netværk
    - Monitorere og administrere et netværks komponenter
    - Opsætte og konfigurere et IDS eller IPS

## Reflektioner over hvad vi har lært

- Med dette har vi på mange måder opfyldt store dele af kravene.
- Vi går nu fra mere praktisk implementering af sikkerhedsforanstaltninger over til projektskrivning.

## Andet