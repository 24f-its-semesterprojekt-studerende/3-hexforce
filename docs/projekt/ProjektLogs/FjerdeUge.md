---
 hide:
#  - footer
---

# Læringslog - fjerde uge (sidste uge)

## Mål for ugen

- **Aflever rapporten.**
- Vurderings afsnit.
- Proces afsnit.
- Diskussion.
- Konklusion.
- Perespektivering (hvis der er noget).
- Formalia.
- Gennemlæsning.

### Praktiske mål

- Alle praktiske mål bliver dele af rapporten vi får skrevet færdigt.
    - Vurderings afsnit.
    - Process afsnit.
    - Diskussions afsnit.
    - Konklusion.
    - Formalia.
    - Gennemlæsning.

**Praktiske opgaver vi har udført**

Ingen for denne uge da det kun har været projekt skrivning.
### Læringsmål
Læringsmålene har været alle dem fra tidligere uger da vi har gået over hele vores projekt imens vi skrev rapporten færdig.

## Reflektioner over hvad vi har lært
- Dette har været et læringsrigt forløb, der kommer til at hjælpe meget under bachelor opgaven. 

## Andet