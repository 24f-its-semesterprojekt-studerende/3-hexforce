---
 hide:
#  - footer
---

# Læringslog - tredje uge

## Mål for ugen

- Rapport skriving
- Implementering af benchmarks

### Praktiske mål
**Praktiske opgaver vi har udført**

- [Implementering af benchmarks](../CISBenchmarks.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

Systemsikkerhed:

- **Viden:**
    - Generelle governance principper / sikkerhedsprocedurer
    - Relevante sikkerhedsprincipper til systemsikkerhed
    - Relevante it-trusler
- **Færdigheder:**
    - Udnytte modforanstaltninger til sikring af systemer
    - Følge et benchmark til at sikre opsætning af enhederne
    - Implementere systematisk logning og monitering af enheder
- **Kompetencer:**
    - håndtere enheder på command line-niveau
    - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
    - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.

Netværkssikkerhed:

- **Viden:**
    - Netværkstrusler
    - Netværk management (overvågning/logning, snmp)
- **Færdigheder:**
    - Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
    - Teste netværk for angreb rettet mod de mest anvendte protokoller
    - Identificere sårbarheder som et netværk kan have.
- **Kompetencer:**
    - Designe, konstruere og implementere samt teste et sikkert netværk
    - Opsætte og konfigurere et IDS eller IPS

## Reflektioner over hvad vi har lært

- Vi er nu færdige med de praktiske dele af opgaven.
- Går nu over til at fokuserer på rapport skrivning.

## Andet