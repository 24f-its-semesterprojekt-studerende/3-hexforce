# Problem formulering: 
Hovedproblem: Hvordan kan man sikre en eller flere usikker applikation(er)/enhed(er), som der er behov for i en virksomhed?

- Hvilke tiltag er nødvendige for at opnå et tilstrækkeligt sikkerhedsniveau?
  - Definer et tilstrækkeligt sikkerhedsniveau.
- Hvilke trussler er der overfor applikationen?
  - Hvilke angrebsmetoder kan der bruges mod applikationen(er)/enhed(er)?
- Hvilke metoder kan bruges til at øge sikkerheden i et system?