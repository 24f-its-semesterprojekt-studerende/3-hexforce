## Requirements:
- Opsat OPNSense
- Opsat Surikata (se tidligere opgave omkring dette.)


## Regl:
- På selve OPNsense VM'en, installer nano (eller anden text editor.)
- lav en fil i /usr/local/etc/suricata/rules/
  - F.eks. kald den "custom.rules"
- Indsæt derefter reglerne.
  - Reglen for at deteketerer CVE-2018-12613:
    - ```alert any any -> 10.33.20.130 any (msg: "Potential RCE exploit"; http.uri; content:"/phpmyadmin/index.php?target="; classtype:bad-unknown; sid:500000; rev:11;)```
  - Gå derefter ind på OPNsense web interface, og reload regler ved at klikke apply. (muligvis nød til at genstarte servicen.)
  - Tilføj reglens sid under "rule adjustment" i "Policy".
  - Reglen burde nu kunne ses under "Administration" i "rules".
    - Tjek evt. log for at se om der er fejl, specifikt med syntax.
    - ![alt text](../projekt/projektbilleder/suricataCVErule.png)
- Lav derefter angrebet (se ExploitGennemgang.md)
- Tjek alert logs:
- ![alt text](../projekt/projektbilleder/IDSAlerts.png)

### IPS
- Under "Services", "Intrusion Detection", "Administration" klik på "Settings".
- Tjek flueben ved "IPS Mode"
  - ![alt text](../projekt/projektbilleder/IPSmode.png)
- Gå derefter til "Rules", og edit den valgte regl til "drop" i stedet for "alert".
  - ![alt text](../projekt/projektbilleder/IPSrule.png)
- Husk at apply alle ændringer.
- Kør derefter exploiten igen.
  - ![alt text](../projekt/projektbilleder/IPSexploit.png)