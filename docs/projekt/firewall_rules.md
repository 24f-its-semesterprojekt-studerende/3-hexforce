# Opsætning af firewall på OPNsense

## OPNsense opsætning
Der er fem interfaces, der er blevet sat op. 

Interface|IP
--|--
00WAN | DHCP  
01MGMT | 10.33.10.1  
02DMZ | 10.33.20.1  
03MONITOR | 10.33.30.1  
04USERS | 10.33.40.1

For at simulere at der er brugere på vores netværk har vi udvidet det med et ekstra interface 04USERS.  
Her skal det forestille sig at virksomhedens brugere er på, som så skal have adgang til 02DMZ, hvor databasen er og 00WAN for at kunne få insternet adgang ud til.

## Firewall regler
### 00WAN
Vi har sat denne firewall for at tillade internet adgang til DMZ. 

Action | Protocol | Source | Dest / Invert | Destination | Dest Port | Description  
--|--|--|--|--|--|--
Pass | IPv4 TCP/UDP | 00WAN net | * | 02DMZ net | * | Allow access to DMZ net  

Dette er Internet ud til WAN, som skal gå igennem DMZ'en først.

### 01MGMT
Dette er virksomhedens Management segment i netværket. Det er her IT administratoren kun har adgang til at tilføje firewalls og pinge andre enehder på andre netværkssegmenter indenfor VLAN. 

Action | Protocol | Source | Dest / Invert | Destination | Dest Port | Description
--|--|--|--|--|--|--
Pass | IPv4+6 TCP/UDP | 01MGMT net | unchecked | 01MGMT address | 53 (DNS) | Allow access to DNS
Block | IPv4 TCP/UDP | 01MGMT net | Unchecked | * | 53 (DNS) | Block access to external DNS
Pass | IPv4 Tcp/UDP | 01MGMT net | unchecked | 10.33.10.100 | (Alias) VPLEPorts | Allow access to graylog on portainer
Pass | IPv4+6 TCP/UDP | 01MGMT net | unchecked | 01MGTM address | 443 (HTTPS) | Allow access to OPNsense web UI
Pass | IPv4+4 TCP/UDP | 01MGMT net | Unchecked | Alias (Wazuh) | 443 (HTTPS) | Allow access to Wazuh Web UI.
Pass | IPv4 TCP/UDP | 01MGTM net | Unchecked | 10.33.30.10 | 9443 | Allow access to PORTAINER host exposed ports
Pass | IPv4 TCP/UDP | 01MGMT net | unchecked | 10.33.30.10 | (Alias) GraylogPorts | Allow access to graylog on portainer
Pass | IPv4 ICMP | 01MGMT net | unchecked | * | * | Allow ICMP (PING)
Pass | IPv4 TCP/UDP | 01MGMT net | checked | (Alias) !PrivateNetworks | * | Allow access only to public networks


Vi har valgt at tilføje disse firewalls for virksomheden fordi det vil simulere at dette netværk står for konfifurationen af VLANnet. Derfor har vi netop valgt at blocke external afgang til port 53 (DNS) for ikke at få udnødvendig eller farlig internet trafik til vores netværk management segment.

### 02DMZ
Dette netværkssegment kaldes for en demilitarized zone

Action | Protocol | Source | Dest / Invert | Destination | Dest Port | Description  
--|--|--|--|--|--|--
Pass | IPv4+6 TCP/UDP | 02DMZ net | unchecked | 02DMZ address | 53 (DNS) | Allow access to DNS
Block | IPv4 TCP/UDP | 02DMZ net | Unchecked | * | 53 (DNS) | Block access to external DNS
Pass | IPv4 TCP/UDP | 02DMZ net | checked | (Alias) !PrivateNetworks | * | Allow access only to public networks
Block | IPv4 TCP/UDP | 02DMZ net | Unchecked | 10.33.20.100/24 | (Alias) VPLEPorts | Block access to VPLEPorts
Pass | IPv4+6 TCP/UDP | 02DMZ net | Unchecked | 00WAN net | * | Allow access for DMZ to be accessed on the internet
Pass | IPv4+6 TCP/UDP | (Alias) WazuhAgentsHosts | unchecked | (Alias) Wazuh | * | Allowing access for Wazuh Agents to communicate with Wazuh Manager

Virksomheden har dette segment for at forhindre at hvert netværkssemgent har adgang til 00WAN og internettet. Det er der smart ved en DMZ er at man får en bedre overblik over netværkstrafik, da hvert netværkssegment, skal få forbindelse igennem DMZ'en.

### 03MONITOR
03MONITOR netværkssegmentet er her, hvor virksomheden vil have deres loggingsystemet såsom Wazuh manager.  
Dette segment kan kun tilgås fra 01MGMT, for at konfigurere og kigge på logs over Wazuh-agenten, som ligger på 02DMZ på enheden 311(Ubuntu 18.04).

Action | Protocol | Source | Dest / Invert | Destination | Dest Port | Description
--|--|--|--|--|--|--
Pass | IPv4+6 TCP/UDP | 03MONITOR net | unchecked | 03MONITOR address | 53 (DNS) | Allow access to DNS
Block | IPv4 TCP/UDP | 03MONITOR net | Unchecked | * | 53 (DNS) | Block access to external DNS
Pass | IPv4 TCP/UDP | 03MONITOR net | checked | (Alias) !PrivateNetworks | * | Allow access only to public networks

Grunden til virksomheden har dette netværkssegment er for at have en logserver. Her er der Wazuh installeret for at kunne logge for evt. angreb eller lave alerts som for eksempel en for gammel PhpMyAdmin version.

### 04USERS
Herunder ses de firewall regler vi indtil videre har tilføjet til 04USERS medarbejder segmentet for virksomheden.  

Action | Protocol | Source | Dest / Invert | Destination | Dest Port | Description
--|--|--|--|--|--|--
Pass | IPv4 TcP/UDP | 04USERS net | * | 02DMZ net | * | Allow access to 02DMZ net 
Pass | IPv4 TCP/UDP | 04USERS net | * | 00WAN net | * | Allow 04USERS access 00WAN  
Block | IPv4 TCP/UDP | * | * | * | 53 (DNS) | Block access to external DNS  
Pass | IPv4 TCP/UDP | 04USERS | * | (Alias) ! PrivateNetworks | * | Allow access to only public Networks  
Pass | IPv4 TCP/UCP | 04USERS net | * | 04USERS address | 53 (DNS) | Allow access to DNS

Grunden til virksomheden har implemeneteret disse firewalls er for eksempel for at medarbejdere har tilladelse til internettet gennem 02DMZ for at kunne bruge PhpMyAdmins webinterface.

### Medarbejdere kan tilgå PhpMyAdmin
For at teste firewall reglen for om medarbejdere kan tilgå 02DMZ netværkssegmentet og kan få fat i hjemmesiden for virksomhedens database, skal vi;

1. Først logge ind med credentials på 401(Ubuntu-desktop-1) som er under 04USERS netværkssegmentet. Dette er en medarbejders computer.
2. Herefter kan tilgå PhpMyAdmin hjemmeside ved at søge på 10.33.20.130/phpmyadmin
3. Efter søgningen ses hjemmesiden, som er den usikre version af PhpMyAdmin.