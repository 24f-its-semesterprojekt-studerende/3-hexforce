### 1.4.1 Ensure bootloader password is set
Tjek: `grep "^set superusers" /boot/grub/grub.cfg`

Respons:
- intet

Tjek: `grep "^password" /boot/grub/grub.cfg`

Respons:
- intet

Remedeation:
- Brug `grub-mkpasswd-pbkdf2` til at lave et kodeord.
- tilføj kodeord og username til /etc/grub.d
- Ændre `CLASS=" i "/etc/grub.d/10_linux` med `--unrestricted`
- update-grub

Respons:
- ???

### 1.4.3 Ensure authentication required for single user mode
Tjek: `grep -Eq '^root:\$[0-9]' /etc/shadow || echo "root is locked"`

Respons:
- intet (rigtigt)

## Authetication, passwords

### 4.3.2 Ensure sudo commands use pty (Automated)
Tjek:
- `grep -rPi '^\h*Defaults\h+([^#\n\r]+,)?use_pty(,\h*\h+\h*)*\h*(#.*)?$' /etc/sudoers*`

Respons:
- intet

Remediation:
- Tilføjede`"Defaults use_pty` til /etc/sudoers

Respons:
- `/etc/sudoers:Defaults use_pty`

### 4.3.4 Ensure users must provide password for privilege escalation (Automated)
Tjek: `grep -r "^[^#].*NOPASSWD" /etc/sudoers*`

Respons:
- intet (korrekt)

### 4.3.5 Ensure re-authentication for privilege escalation is not disabled globally (Automated)
Tjek: `grep -r "^[^#].*\!authenticate" /etc/sudoers*`

Respons:
- intet (korrekt)

### 4.3.6 Ensure sudo authentication timeout is configured correctly (Automated)
Tjek: `grep -roP "timestamp_timeout=\K[0-9]*" /etc/sudoers*`

Respons:
- Intet

Tjek `sudo -V | grep "Authentication timestamp timeout:"`

Respons:
- Authentication timestamp timeout: 15.0 minutes

### 4.3.7 Ensure access to the su command is restricted (Automated)
Tjek: `grep -Pi '^\h*auth\h+(?:required|requisite)\h+pam_wheel\.so\h+(?:[^#\n\r]+\h+)?((?!\2)(use_uid\b|group=\H+\b))\h+(?:[^#\n\r]+\h+)?((?!\1)(use_uid\b|group=\H+\b))(\h+.*)?$' /etc/pam.d/su`

Respons:
- intet

Remediation:
- `groupadd sugroup`
- i /etc/pam.d/su:
  - `auth required pam_wheel.so use_uid group=sugroup`

Tjek: (samme som før)

Respons: `auth required pam_wheel.so use_uid group=sugroup`

Tjek: `grep <group_name> /etc/group`

Respons:
- `sugroup:x:1001:`

### 4.4.1 Ensure password creation requirements are configured (Automated)
- pwquality ikke installeret, startede ud med det.

Tjek: `grep '^\s*minlen\s*' /etc/security/pwquality.conf`

Respons:
- intet
Tjek: `grep '^\s*minclass\s*' /etc/security/pwquality.conf`

Respons:
- intet

Tjek: `grep -P '^\h*password\h+[^#\n\r]+\h+pam_pwquality\.so\b' /etc/pam.d/common-password`

Respons:
- `password        requisite                       pam_pwquality.so retry=3`

Remediation:
- Ændre `minlen` til 14 i /etc/security/pwquality.conf
- Ændre `minclass` til 4 i /etc/security/pwquality.conf

Tjek: `grep '^\s*minlen\s*' /etc/security/pwquality.conf`

Respons:
- `minlen = 14`

Tjek: `grep '^\s*minclass\s*' /etc/security/pwquality.conf`

Respons:
- `minclass = 4`'

### 4.5.1.1 Ensure minimum days between password changes is configured (Automated)
Tjek: `grep PASS_MIN_DAYS /etc/login.defs`

Respons:
- `PASS_MIN_DAYS   Minimum number of days allowed between password changes. PASS_MIN_DAYS   0`

Tjek: `awk -F : '(/^[^:]+:[^!*]/ && $4 < 1){print $1 " " $4}' /etc/shadow`

Respons:
- `root 0
mikkel 0`

Remediation: Ændre `PASS_MIN_DAYS` til 1 /etc/login.defs
`chage --mindays 1 root`
`chage --mindays 1 mikkel`

Tjek: `awk -F : '(/^[^:]+:[^!*]/ && $4 < 1){print $1 " " $4}' /etc/shadow`

Respons:
- intet

### 4.5.1.2 Ensure password expiration is 365 days or less
Tjek: `grep PASS_MAX_DAYS /etc/login.defs`

Respons:
- `PASS_MAX_DAYS   Maximum number of days a password may be used. PASS_MAX_DAYS   99999`

Tjek: `awk -F: '(/^[^:]+:[^!*]/ && ($5>365 || $5~/([0-1]|-1|\s*)/)){print $1 " " $5}' /etc/shadow`

Respons:
- `root 99999  
mikkel 99999`

Remediation: Ændre `PASS_MAX_DAYS` til 365 /etc/login.defs
`chage --maxdays 365 root`
`chage --maxdays 365 mikkel`

Tjek: `awk -F : '(/^[^:]+:[^!*]/ && $4 < 1){print $1 " " $4}' /etc/shadow`

Respons:
- intet

### 4.5.1.3 Ensure password expiration warning days is 7 or more
Tjek: `grep PASS_WARN_AGE /etc/login.defs`

Respons:
- `PASS_WARN_AGE   Number of days warning given before a password expires. PASS_WARN_AGE   7`

Tjek: `awk -F: '(/^[^:]+:[^!*]/ && $6<7){print $1 " " $6}' /etc/shadow`

Respons:
- intet (rigtigt)

### 4.5.1.4 Ensure inactive password lock is 30 days or less
Tjek: `useradd -D | grep INACTIVE`

Respons:
- `INACTIVE=-1`
Tjek: `awk -F: '(/^[^:]+:[^!*]/ && ($7~/(\s*|-1)/ || $7>30)){print $1 " " $7}' /etc/shadow`

Respons:
  - `root  
mikkel`  

Remediation:
- `useradd -D -f 30`
- `chage --inactive 30 root`, `chage --inactive 30 mikkel`

Tjek: `useradd -D | grep INACTIVE`

Respons:
- `INACTIVE=30`

Tjek: `awk -F: '(/^[^:]+:[^!*]/ && ($7~/(\s*|-1)/ || $7>30)){print $1 " " $7}' /etc/shadow`

Respons:
- Intet (rigtigt)

### 4.5.1.5 Ensure all users last password change date is in the past
Tjek: kør script

Respons:
```
- Audit Result:
 ** PASS **
- All user password changes are in the past
```

### 4.5.1.6 Ensure the number of changed characters in a new password is configured
Tjek: `grep -P '^\h*difok\h*=\h*([2-9]|[1-9][0-9]+)\b' /etc/security/pwquality.conf`

Respons:
- intet

Remediation:
- Ændre `difok` til 2 i /etc/security/pwquality.conf

Tjek: `grep -P '^\h*difok\h*=\h*([2-9]|[1-9][0-9]+)\b' /etc/security/pwquality.conf`

Respons:
- `difok = 2`

### 4.5.1.7 Ensure password dictionary check is enabled
Tjek: `grep -Pi -- '^\h*dictcheck\h*=\h*0\b' /etc/security/pwquality.conf`

Respons:
- intet (rigtigt)

### 4.5.7 Ensure maximum number of same consecutive characters in a password is configured
Tjek: `grep -Pi '^\h*maxrepeat\h*=\h*[1-3]\b' /etc/security/pwquality.conf`

Respons:
- intet
Remediation:
- Sæt `maxrepeat` til 3 i /etc/security/pwquality.conf

Tjek: `grep -Pi '^\h*maxrepeat\h*=\h*[1-3]\b' /etc/security/pwquality.conf`

Respons:
- maxrepeat = 3

## Logging
>This section will cover the minimum best practices for the usage of either rsyslog or journald. The recommendations are written such that each is wholly independent of each other and only one is implemented

### 5.1.2.1 Ensure rsyslog is installed
Tjek: `dpkg-query -s rsyslog &>/dev/null && echo "rsyslog is installed"`

Respons:
- `rsyslog is installed.`

### 5.1.2.2 Ensure rsyslog service is enabled
Tjek: `systemctl is-enabled rsyslog`

Respons:
- `enabled`

### 5.1.2.3
Vi bruger ikke journald og ser derfor ikke denne benchmark som relevant.

### 5.1.2.4 Ensure rsyslog default file permissions are configured
Tjek: `grep ^\$FileCreateMode /etc/rsyslog.conf /etc/rsyslog.d/*.conf`

Respons:
- `/etc/rsyslog.conf:$FileCreateMode 0640`

### 5.1.2.5 Ensure logging is configured
Tjek: `ls -l /var/log/`

Respons:
![alt text](../projekt/projektbilleder/remediation5125.png)

### 5.1.2.6 Ensure rsyslog is configured to send logs to a remote log host
Tjek: `grep "^*.*[^I][^I]*@" /etc/rsyslog.conf /etc/rsyslog.d/*.conf`

Respons:
- intet

Remediation:
- Graylog bruges som vores logging server.
- https://go2docs.graylog.org/5-2/getting_in_log_data/syslog_inputs.html

Tjek: `grep "^*.*[^I][^I]*@" /etc/rsyslog.conf /etc/rsyslog.d/*.conf`

Respons: 
`/etc/rsyslog.conf:*.*@10.33.30.10:514;RSYSLOG_SyslogProtocol123Format`

### 5.1.2.7 Ensure rsyslog is not configured to receive logs from a remote client
Tjek: `grep '$ModLoad imtcp' /etc/rsyslog.conf /etc/rsyslog.d/*.conf`

Respons:
- intet (rigtigt)

Tjek: ` grep '$InputTCPServerRun' /etc/rsyslog.conf /etc/rsyslog.d/*.conf`

Respons:
- intet (rigtigt)

### 5.2.1.1 Ensure auditd is installed
Tjek: `dpkg-query -s auditd &>/dev/null && echo "auditd is installed"`

Respons:
- intet

Remediation:
- installere auditd `apt install auditd`

Tjek: `dpkg-query -s auditd &>/dev/null && echo "auditd is installed"`

Respons:
- `auditd is installed`

### 5.2.1.2 Ensure auditd service is enabled and active
Tjek: `systemctl is-enabled auditd`

Respons:
- `enabled`

### 5.2.1.4 Ensure audit_backlog_limit is sufficient
Tjek: `find -L /boot -type f -name 'grub.cfg' -exec grep -Ph -- '^\h*linux\b' {} + | grep -Pv 'audit_backlog_limit=\d+\b'`

Respons:
```
linux   /vmlinuz-4.15.0-213-generic root=/dev/mapper/ubuntu--vg-ubuntu--lv ro  maybe-ubiquity
linux   /vmlinuz-4.15.0-213-generic root=/dev/mapper/ubuntu--vg-ubuntu--lv ro  maybe-ubiquity
linux   /vmlinuz-4.15.0-213-generic root=/dev/mapper/ubuntu--vg-ubuntu--lv ro recovery nomodeset dis_ucode_ldr
```

Remedidation:
- tilføj `audit_backlog_limit=8192` til GRUB_CMDLINE_LINUX
- Derefter `update-grub`

Tjek: `find -L /boot -type f -name 'grub.cfg' -exec grep -Ph -- '^\h*linux\b' {} + | grep -Pv 'audit_backlog_limit=\d+\b'`

Respons:
- intet (rigtigt)

### 5.2.2.1 Ensure audit log storage size is configured
Tjek: `grep -Po -- '^\h*max_log_file\h*=\h*\d+\b' /etc/audit/auditd.conf`
Respons:
- max_log_file = 8`

### 5.2.2.2 Ensure audit logs are not automatically deleted
Tjek: `grep max_log_file_action /etc/audit/auditd.conf`
Respons:
- `max_log_file_action = ROTATE`
Remediation:
- set `max_log_file_action` til `keep_logs` i /etc/audit/auditd.conf
Tjek: `grep max_log_file_action /etc/audit/auditd.conf`
Respons:
- `max_log_file_action = keep_logs`

### 5.2.2.3 Ensure system is disabled when audit logs are full
Tjek: `grep space_left_action /etc/audit/auditd.conf`

Respons:
- `space_left_action = SYSLOG
admin_space_left_action = SUSPEND`

Tjek: `grep action_mail_acct /etc/audit/auditd.conf`

Respons:
- `action_mail_acct = root`

Tjek: `grep -E 'admin_space_left_action\s*=\s*(halt|single)' /etc/audit/auditd.conf`

Respons:
- intet

Remediation:
- set `space_left_action` til email i /etc/audit/auditd.conf
- set `admin_space_left_ation` til half i /etc/audit/auditd.conf

Tjek: `grep space_left_action /etc/audit/auditd.conf`

Respons:
- `space_left_action = email
admin_space_left_action = halt`

Tjek: `grep -E 'admin_space_left_action\s*=\s*(halt|single)' /etc/audit/auditd.conf`

Respons:
- `admin_space_left_action = halt`


### Ikke implementeret
#### 5.2.3.15
???

#### 5.2.3.16
???

#### 5.2.3.17
???

#### 5.2.3.18
???

### 6.2.2 Ensure /etc/shadow password fields are not empty
Tjek: `awk -F: '($2 == "" ) { print $1 " does not have a password "}' /etc/shadow`

Respons:
- intet (rigtigt)