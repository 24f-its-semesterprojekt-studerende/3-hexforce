# Log
### 07/05
- Opsat nyt netværk (04USERS, 10.33.40.0/24), til brug for "brugere" på netværket.
- Problemformulering og feedback herfor
- Speficiering af afgrænsning og projekts emne.
#### Plan for onsdag 08/05
- Opsætning af PhpmyAdmin
- Hvis der er mere tid, begynder vi at exploit eller mitigeringer.
- Efter opsætningen, kigger vi på Issues i Gitlab.

### 08/05
- Opsatte PHPMyAdmin
  - Først prøvede vi docker container, havde dog problemer i.fht. exploit.
  - Skiftede til en guide der gik over at sætte en VM op der var sårbar.
  - Skiftede derefter til ældre version af Ubuntu.
  - Fik CVE exploit til at virke, fik adgang gennem authorized user.
  - I morgen er det helligdag :).
  - Fredag laver vi netværk.

### 10/05
- Opsat loggning for mysql
  - general_log
- Fundet payload fra metasploit's RCE angreb
- ![alt text](../projekt/projektbilleder/metasploitpayload.png)
- Firewall regler er arbejdet på
- Brugt tid på at sætte Wazuh decoders op, men Wazuh har indbygget detektering.
- Arbejdet på rapport.

### 13/05
- Snakket med Martin og Nikolaj.
- Sat IDS op på OPNSense.
- Skrevet i metode afsnit, klartgjort hvad der burde være der.
- I morgen: kigget på opsætningen af IPS i OPNSense, skrevet videre (helst færdigt) metode afsnittet, og indledning.

### 14/05
- Guide til IDS, IPS, exploit
- Fixet netværk ting
- Fixet graylog
- Skrevet i rapporten
  - Metode afsnit og indledning
  - 2193
- Firewall regler skrevet ned
- Til i morgen: aller sidste på indledning, gjort metode afsnittet færdigt.
  - Benchmarks?
  - Burde nok rydde lidt op i metode afsnittet.

### 15/05
- Skrevet i rapport
- Ryddet op i rapport
- 3166
- Sendt afkast afsted
- Ryddet op i issues på Gitlab.
- Til i morgen: begynde på implementering af benchmarks.

### 17/05
- Sendt afkast afsted (lidt for sent)
- Arbejdet med benchmarks
  - Fået implementeret nogle af dem (se CISBenchmarks.md)
- Arbejdet med logning (fået syslog fra ubuntu18 over på Graylog)
- 3272
- Til tirsdag:
  - Færdig med benchmarks, begyndt på analyse/implementerings afsnittet

### 21/05
- Begyndt på analyse afsnit.
- Snakket med Martin og Nikolaj
  - Ændringer i rapporten
- Proces afsnit
  - gennemgang i markdown filer
- 3915
- Til onsdag:
  - Skrevet videre i analyse afsnit
  - Find måder at gøre metode afsnittet mindre "wall of text"-y (billeder, grafer, etc. etc.)

### 22/05
- Arbejdet videre på analyse afsnit.
- Smukke billeder!!
- Formalia ting
- 4854
- Til torsdag
  - Skriver hjemmefra
  - Opdelt afsnit til at skrive på hjemme.

### 23/05
- Skrevet i rapporten
- 5392

### 24/05
- Skrevet i rapporten.
- Burde være færdige med metode/analyse afsnittet.
- Til næste gang:
  - Start på vurderings afsnittet.
  - Start på proces afsnittet.
    - Video?

### 27/05
- Færdig med vurderings afsnittet.
- Begyndt på process afsnittet.
- Ingen video i dag.
- Til i morgen:
  - Video? Eller laver vi det derhjemme og kobler det sammen efterfølgende?
    - Rasmus laver gennemgang af exploit.
    - Mikkel laver overordnet gennemgang af setup på proxmox
  - Process, diskussions afsnit.
    - Med video burde vi være tæt på at være færdig med proces afsnit.
  - Kig noget mere på vores projekt styring.

### 28/05
- Video mangler firewall regler etc.
- Skrevet på konklusion, diskussion.
- Nogle ændringer i firewall/netværksegmentering segmentet.
- Til i morgen:
  - Video færdig, proces afsnit færdig.
  - Konklusion færdig, diskussions afsnit færdig.
  - Formalia, referencer, etc.
  - Gået igennem alt det røde tekst.

### 29/05
- Video.
- Formalia.
- Tjekket alle vores referencer igennem.
- Gennemlæsning.
  - Og brugt Words indbygget text editor program.
- Oprettet grupper på wiseflow.
- Opgavens struktur er også skrevet.
- Færdig? Bliver hjemme i morgen, kigger opgaven over igen.