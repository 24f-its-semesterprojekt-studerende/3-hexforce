---
 hide:
#   - footer
---

# Dokumentation af opsætning af server
2024/02/02
## Information

- Dette dokument går over vores opsætning af ProxMox på en Dell 5820 server. Inkluderet er billeder og de steps vi gjorde for at få det hele op at køre.

## Instruktioner
### BIOS

brug Displayport 1

For at komme i BIOS  tryk F2 efter genstart, der vil så "preparing to enter setup" oppe i højre hjørne.

Der kræves et kodeord
![Ram konfiguration](../images/ProxMoxSetup/Dell_5820_BIOS_password.jpg)

du vil nu se dette billede
![Ram konfiguration](../images/ProxMoxSetup/Dell_5820_BIOS_settings.jpg)

general-> systeminformation
- 2x8GB
- 512GB SSD
- LOM MAC Address D8-9E-F3-37-07-27
LOM: LAN on motherboard

![Dell_5820_system_information_RAM](../images/ProxMoxSetup/Dell_5820_system_information_RAM.jpg)
![Dell_5820_system_information_SSD](../images/ProxMoxSetup/Dell_5820_system_information_SSD.jpg)

sluk computer og hiv stikket ud
### Setup af hardware
Nyt Hardware  
![Dell_5280_Nyt_hardware](../images/ProxMoxSetup/Dell_5820_Nyt_hardware.jpg)  
tag låget af
se manual side 19
#### RAM
fjern airshroud
se manual side 50

fjern RAM
Se manual side 52
![Dell_5820_RAM_tab](../images/ProxMoxSetup/Dell_5820_RAM_tab.jpeg)
![Dell_5820_gammel_ram](../images/ProxMoxSetup/Dell_5820_gammel_RAM.jpg)

Sæt RAM i DIMM1 og DIMM2 efter RAM konfiguration Se manual side 80

![Ram konfiguration](../images/ProxMoxSetup/Manual_RAM_konfiguration.png)

sæt airshroud på igen
#### SSD
Fjern HDD bezel se manual side 23

![Dell_5820_uden_front_panel](../images/ProxMoxSetup/Dell_5820_uden_front_panel.jpg)

Fjern harddisk carrier se manual side 25

![Dell_5820_SSD_i_harddisk_carrier](../images/ProxMoxSetup/Dell_5820_SSD_i_harddisk_carrier.jpg)

fjern SSD fra harddisk carrier se manual side 27

installer de nye ssd i harddisk carrier og sæt dem i computeren

Gå til BIOS for at se om computeren finder alt de bye hardware
![Dell_5820_system_information_RAM](../images/ProxMoxSetup/Dell_5820_system_information_RAM_ny.jpg)
![Dell_5820_system_information_SSD](../images/ProxMoxSetup/Dell_5820_system_information_SSD_ny.jpg)

### installer OS
insæt USB og genstart så du ser "Welcome to Proxox Virtual Environment"

![proxmox_installer](../images/ProxMoxSetup/proxmox_installer.jpg)

"install Proxmox VE (graphical)"


hostname 
ipadresse 10.56.16.33/22
gateway 10.56.16.1
dns server 1.1.1.1
![proxmox_setup_instillinger](../images/ProxMoxSetup/proxmox_setup_instillinger.jpg)

ping 1.1.1.1
![proxmox_ping](../images/ProxMoxSetup/proxmox_ping.jpg)

gå i en browser
gå til 10.56.16.33:8006

login, du skulle nu se interfaces som vises under.
![proxmox_interface](../images/ProxMoxSetup/proxmox_interface.png)


## Links
[Dell 5820 Manual](https://dl.dell.com/content/manual32840811-dell-precision-5820-tower-owner-s-manual.pdf?language=en-us)