---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semesterprojekt gruppe 03 'HexForce'

Gruppe 3 består af Rasmus, Mikkel og Tobias.

På dette website finder du projektplaner, læringslogs og andet relateret til semesterprojektet

Læringslogs og link til dokumentation skal inkluderes som bilag i projektrapporten (eksamensaflevering).  

Procesafsnittet i rapporten skal beskrive de enkelte ugers projektarbejde med henvisning til logs (link til relevante dele af denne gitlab side)

Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)

Links til de andre grupper:

Gruppe 1. [ByteShield](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/)  
Gruppe 2. [CryptoHex](https://24f-its-semesterprojekt-studerende.gitlab.io/2-cryptohex/)  
Gruppe 3. Du er her  
Gruppe 4. [SecMinds](https://24f-its-semesterprojekt-studerende.gitlab.io/4-secminds/)  
Gruppe 5. [HexDef](https://24f-its-semesterprojekt-studerende.gitlab.io/5-hexdef/)  
Gruppe 6. [CipherCrew](https://24f-its-semesterprojekt-studerende.gitlab.io/6-ciphercrew/)  
Gruppe 7. [CodeCrypts](https://24f-its-semesterprojekt-studerende.gitlab.io/7-codecrypts/)  
