# Uge 14 - Auditd

## Opgave 26 - Installer auditd

Auditd kan bruges til at overvåge filer, mapper og systemkald.

Installer auditd på ubuntu med `apt install auditd`.  
Tjek at at auditd køre med `systemctl status auditd`.  
Udskriv de nuværende regler med `auditctl -l`.  
Udskriv logfilen med `cat /var/log/audit/audit.log`.  

## Opgave 27 - Audit af en fil for ændringer


Audit regler skal enten oprettes ved at bruge `auditctl` eller ved at tilføje regler i filen `/etc/audit/audit.rules`.

### Tilføj regler med `auditctl`

- regel til når `etc/passwd` ændres
    - `auditctl -w /etc/passwd -p wa -k user_change`´
        - `-w`, *where* hvilken fil reglen skal gælde for.
        - `-p`, *permissions*, hvilke rettigheder skal overvåges.
            - `wa`, her overvåges *write* og *attribute. Altså, vhis der skrive til files, eller filens metadata ændres.
        - `-k`, *key*, en key der kan søges på.
- Søg på de hændelser med `aureport -i -k | grep user_change`.
    - Her er formattet `Datetime Key Success Executing Event (process, bruger ProcessID)`

### Gem regler
De regler der bliver sat med `auditctl` er volatile. Derfor skal reglerne gemmes i `/etc/audit/rules.d/` 

I filen `/etc/audit/audit.rules` ses alle regler som er blevet autogenereret ud fra `/etc/audit/rules.d/`

De regler der blev oprettet med `auditctl -l` kan gemmes med `sh -c "auditctl -l > /etc/audit/rules.d/custom.rules"`.

### Audit alle filændringer

- brug `aureport -f`
 - `-i`, for at skrive brugernavn i stedet for bruger ID.

## Opgave 28 - Audit af et directory

For at lave en audit på et directory brug kommandoen `auditctl -w <sti> -k directory_watch_rule`.

## Opgave 29 - Overvågning af OS API'et for specifikke systemkald
Auditd kan også bruges til at overvåge kommunikationen mellem kørende processer og operativsystemet. 

### Overvågning af processer, der bliver afsluttet
`auditctl -a always,exit -F arch=b64 -S kill -F key=kill_rule`