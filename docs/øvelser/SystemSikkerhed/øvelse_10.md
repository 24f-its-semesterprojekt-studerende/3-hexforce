# Opgave 10 - Bruger rettigheder i Linux
Bruger rettigheder deles op i tre segmenter

- Ejeren af filens rettigheder
- gruppen som er tilknyttet filens rettigheder
- Alle andres rettigheder til filen

Der er tre rettigheder som kan tildeles filen:

- Read (r)| Læse og kopirer
- Write (w)| Skrive og ændre filen
- Execute (x)| eksekvere filen

![alt text](images/øvelse_10/image.png)

med `ls -l` kan vi se filens rettigheder.

Til venstre fra filen er der ti `-`

Den første viser om det er et directory. De næste tre viser rettighederne for ejeren, de næste tre igen viser rettighederne for gruppen og de sidste tre viser for andre

Som set på billedet har ejeren read og write, gruppen har read og write og andre har read rettigheder.

For at ændre rettigheder brug `chmod`

- user (u), group(g), other (o)
- `-` for at fjerne, `+` for at tilføje
- read (r), write(w), execute (x)
- Eksempel
    - `chmod go-rwx file`
        - Fjern read write og execute rettigheder fra `group` og `other`
    - Det samme kan gøres med: `chmod -777 file`

| værdi | rettighed | sum   |
| ----- | --------- | ----- |
| 0     | ---       | 0+0+0 |
| 1     | --x       | 0+0+1 |
| 2     | -w-       | 0+2+0 |
| 3     | -wx       | 0+2+1 |
| 4     | r--       | 4+0+0 |
| 5     | r-x       | 4+0+1 |
| 6     | rw-       | 4+2+0 |
| 7     | rwx       | 4+2+1 |
