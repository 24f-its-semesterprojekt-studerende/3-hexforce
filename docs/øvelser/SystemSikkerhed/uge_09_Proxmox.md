# Uge 09 - Opsætning at Ubuntu server på Proxmox
Der skal oprettes to hosts på Proxmox med følgende specifikationer

| Host         | Description                        | CPU Cores | HDD  | RAM |
| ------------ | ---------------------------------- | --------- | ---- | --- |
| Wazuh server | Host running Wazuh SIEM/XDR system | 4         | 80GB | 8GB |
| Target host  | Host being monitored by Wazuh      | 2         | 30GB | 4GB |


Opret en bruger til hvert gruppe medlem på VM'erne.

Giv hvert gruppemedlems bruger sudo rettigheder, ved at tilføje dem til sudo gruppen. Du kan finde vejledning her

Tilføj hver gruppemedlem med `adduser`, ikke `useradd`.

Giv brugerne sudo rettigheder:
`usermod -aG sudo user`

Verificer med `groups user`, hvis brugeren er sudo user skriver en `<user> sudo users`. Som det ses på billedet.  
![alt text](images/uge_09_ubuntu/image.png)

Det er god praksis ikke at tillade root login på sin VM, og gruppen bør derfor også sikre at der ikke længere kan logges ind som root. På Ubuntu server kan dette gøres med kommandøn `sudo passwd -l root`
