# Opgave
Med udgangspunkt i vores efterforsknings proces, skal i danne og beskrive en proces i tilfælde af kompromittering af en host.  
(Angriber er lykkes med escalation of privilege angreb, og har opnået superbruger rettigheder ).  
Husk og hold det konceptuelt


## Hændelse
- Angriber er lykkes med escalation of privilege angreb, og har opnået superbruger rettigheder.

## Identificering

- Hvem
    - Se på hvad der er blevet angrebet (server), og ud fra det underrette dem der er påvirket (CIA).
- Hvad
    - Angriber er lykkes med escalation of privilege angreb, og har opnået superbruger rettigheder.
- Hvor
    - En server
        - Hvor er den (i netværket).
            - Hvad har den adgang til, info og til andre dele af netværket. 

## Isolering af systemet

- Se muligheder for at isolere serveren fra andre dele af netværket (hvis muligt).

## Imaging

- Kopi af alle relevante data'er.

## Hashing

- Sikre integriteten af log filer ved brug af hashing.
    - Og andre relevante filer.

## Analyse

- Kig logs igennem
    - Brug viden til at stoppe denne angrebs metode i fremtiden.

## Melding

- Meld til de relevante personer og myndigheder.