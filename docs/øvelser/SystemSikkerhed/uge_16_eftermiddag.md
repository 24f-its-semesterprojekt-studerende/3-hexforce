# Uge 16 - Eftermiddags opgave - 

## Vælg en POC til detektering

### File integrity monitoring

Tilføj `<directories check_all="yes" report_changes="yes" realtime="yes">/home/ubuntu</directories>` til `/var/ossec/etc/ossec.conf`

![ossec.conf fil](images/uge_16/image.png)

Restart agentent `sudo systemctl restart wazuh-agent`
/home/ubuntu

Når der tilføjes ændres eller slettes en fil vil de ses i Wazuh security events.

![Security events](images/uge_16/image-1.png)

Når man kigger ned i en event kan man se hvilken fil det drejer sig om.
![Security events detaljer](images/uge_16/image-2.png)

### Detecting suspicious binaries
Tjek at configurationsfilen har følgende `<rootcheck>` block. Som default skulle den være enabled.

```xml
<rootcheck>
    <disabled>no</disabled>
    <check_files>yes</check_files>

    <!-- Line for trojans detection -->
    <check_trojans>yes</check_trojans>

    <check_dev>yes</check_dev>
    <check_sys>yes</check_sys>
    <check_pids>yes</check_pids>
    <check_ports>yes</check_ports>
    <check_if>yes</check_if>

    <!-- Frequency that rootcheck is executed - every 12 hours -->
    <frequency>43200</frequency>
    <rootkit_files>/var/ossec/etc/shared/rootkit_files.txt</rootkit_files>
    <rootkit_trojans>/var/ossec/etc/shared/rootkit_trojans.txt</rootkit_trojans>
    <skip_nfs>yes</skip_nfs>
</rootcheck>
```


Rootcheck scanner hver 12 time og kan force skanne, hvis man restarter wazuh-agenten med:
`sudo systemctl restart wazuh-agent`


Når vi har udført angrebet, som herover og genstartet wazuh-agenten for at kunne se angrebet på Wazuh, tilgår vi wazuh webinterface og søger på rule.id:510.
![Wazuh security events rule:id 510](images/uge_16/image-3.png)


## Eftermiddags opgave - Vælg en dektektering fra Wazuh blogs

### Her har vi valgt Detecting Keyloggers (T1056.001) on Linux endpoints bloggen.

Keyloggere er spyware, der overvåger og registrerer brugertastetryk på slutpunkter.

Denne angrebsmetode er på MITRE ATT&CK database under taktikken Input Capture(T1056.001).

Følg bloggen på (https://wazuh.com/blog/detecting-keyloggers-on-linux-endpoints/)[https://wazuh.com/blog/detecting-keyloggers-on-linux-endpoints/]

### Configuration
Første skridt var at sætte logging af passwords op:
```bash
if sudo test -f /etc/pam.d/password-auth; then sudo cp /etc/pam.d/password-auth /tmp/password-auth.bk; fi;
if sudo test -f /etc/pam.d/system-auth; then sudo cp /etc/pam.d/system-auth /tmp/system-auth.bk; fi;
sudo echo "session    required    pam_tty_audit.so enable=* log_password" >> /etc/pam.d/password-auth
sudo echo "session    required    pam_tty_audit.so enable=* log_password" >> /etc/pam.d/system-auth
```

Og derefter gøre så alle nye commands i shell bliver også logged:

```bash
PROMPT_COMMAND='history -a >(tee -a ~/.bash_history |logger -t "$USER[$$] $SSH_CONNECTION ")'
echo "\$PROMPT_COMMAND=$PROMPT_COMMAND"
tail /var/log/syslog
```

Vi testede derefter vores konfiguration ved brug af:
```bash
echo "$PROMPT_COMMAND"; echo "testing the simulation"
cat /var/log/{syslog,messages} | grep "testing the simulation"
```

### Angreb

Herefter simulerede vi en aktiv keylogger ved brug af:
```bash
cp /usr/bin/sleep ~/lkl; ~/lkl 600 &
```

### Wazuh

Nu kunne vi genstarte vores Wazuh agent
```bash
systemctl restart wazuh-agent
```

Og gå ind i Wazuh for at se alerts.

![Wazuh dashboard](images/uge_16/image-4.png)