# Øvelse 48 - Mitre ATT&CK Taktikker, Teknikker, mitigering & detektering

Formålet med denne øvelse er at udforske Mitre ATT&CK-rammeværket med fokus på taktikken Privilege Escalation (Forhøjelse af Privilegier), teknikken T1548, samt afhjælpningen M1026 & Detekteringen DS0022.


[Præsentation](Øvelse%2048%20præsentation.pdf)

linket over er en presentation har undersøgt nedestående spørgsmål

- Undersøg Mitre ATT&CK taktikken TA0004. Hvad betyder denne taktik?
- Identificer specifikke under-teknikker under T1548 og hvordan de bidrager til at opnå Privilege Escalation.
- Identificer og undersøg Mitigeringen M1026.
- Identificer og undersøg detekteringen DS0022.
- Til næste uge, forbered en kort præsentation, der opsummerer dine resultater, herunder en forklaring på Privilege Escalation-taktikken, teknikkerne under T1548, mitigeringen M1026 samt detekteringen DS0022.
