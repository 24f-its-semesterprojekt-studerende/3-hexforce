# Opgave 18.1 - Eftermiddagsopgave med webserver på Proxmox-serveren

Der er blevet opsat en Apache2-webserver og en NGINX- webverser.

Apache2-webserveren er blevet opsat ved at følge denne [guide](https://ubuntu.com/tutorials/install-and-configure-apache#5-activating-virtualhost-file)

NGINX-webserveren er blevet opsat ved at følge denne [guide](https://ubuntu.com/tutorials/install-and-configure-nginx#3-creating-our-own-website)


Disse to enheder er placeret i 02DMZ segmentet, som set på netværksdiagramet under. Der blev efterfølgende sat en Wazuh agent op på NGINX. Dette blev også gjort på Apache hosten i [Øvelse 25.2](øvelse_25.2.md)

![Netværksdiagram](../netværk/images/øvelse%2080/Netværksdiagram.drawio.svg)

Efter agents er sat op kan de ses i Wazuh webinterface.  
![Wazuh dashboard med to aktive agents](images/øvelse_18.1/image.png)


Tabelen under viser alle VM instanser på vores Proxmox

| Host                  | Description                        | CPU Cores | HDD  | RAM |
| ------------          | ---------------------------------- | --------- | ---- | --- |
| Wazuh server          | Host running Wazuh SIEM/XDR system | 4         | 80GB | 8GB |
| Target host Apache    | Host being monitored by Wazuh      | 2         | 30GB | 4GB |
| Target host NGINX     | Host being monitored by Wazuh      | 1         | 16GB | 2GB |
| VPLE                  | Pentesting environment             | 2         | ?    | 1GB |
| Kali                  | Management host                    | 2         | 32GB | 4GB |
| Docker                | Graylog                            | 2         | 32GB | 4GB |
| OPNsense              | Firewall                           | 2         | 32GB | 8GB |