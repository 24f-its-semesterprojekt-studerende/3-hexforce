# Øvelse 30 - Checksum af en fil

Lav en checksum af en fil med: `sha256sum <fil>`

![Skærm billede ubuntu](images/øvelse_30/image.png)