# Øvelse 47 - CIS kontroller

1. Hvordan vurderer du BS Consulting A/S' nuværende cybersikkerhedspraksis i forhold til de forskellige CIS 18 kontroller?
    - Kontrol 1:
        - De må have en vis management for at kunne køre opdateringer på deres enheder, men om den er fyldestgørende vides ikke.
    - Kontrol 2:
        - De har implementeret antivirus- og antimalware-software på alle deres systemer og foretager regelmæssige opdateringer. 
    - Kontrol 3:
        - De har følsomme kundedata på deres systemer.
    - Kontrol 4:
        - De har implementeret antivirus- og antimalware-software på alle deres systemer og foretager regelmæssige opdateringer. 
    - Kontrol 5:
        - Der er en adgangskontrolpolitik på plads, som begrænser brugeradgang til systemer baseret på deres roller og ansvarsområder.
    - Kontrol 6:
        - Der er en adgangskontrolpolitik på plads, som begrænser brugeradgang til systemer baseret på deres roller og ansvarsområder.
    - Kontrol 7:
        - Virksomheden har en reaktiv tilgang til cybersikkerhed, hvor de kun reagerer på hændelser, når de opstår, i stedet for at have proaktive sikkerhedsforanstaltninger på plads.
    - Kontrol 8:
        - Siden de har en reaktiv tilgang, ville de højst sandsynligvis gøre brug af audit log efter et angreb er taget sted.
    - Kontrol 9
        - Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.
    - Kontrol 10:
        - Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.
        - De har implementeret antivirus- og antimalware-software på alle deres systemer og foretager regelmæssige opdateringer.
    - Kontrol 11:
        - Reaktiv tilgang kan betyde at de ikke kører nødvendige backups regelmæssigt.
    - Kontrol 12
        - De kører regelmæssigt opdateringer, som kan forhindre threat aktør i at bruge en exploit til at få adgang.
    - Kontrol 13:
        - Reaktiv tilgang kan betyde at de ikke har nogen monitoring på deres netværk, hvilket vil gøre det svært at se hvis det er nogen der prøver at få adgang, eller teste, på deres netværk.
    - Kontrol 14:
        - Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.
    - Kontrol 15:
        - Da de har en reaktiv tilgang, betyder det sandsynligvis at de ikke har noget supply chain protection.
    - Kontrol 16:
        - De foretager regelmæssigt opdatering.
    - Kontrol 17:
        - “[...] hvor de kun reagerer på hændelser.”
        - Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.
    - Kontrol 18:
        - Reaktiv tilgang betyder at de sandsynligvis ikke har udført nogen red-team testing på deres systemer.
2. Hvilken implementeringsgruppe (IG) mener du, BS Consulting A/S hører til, og hvorfor?
    - De burde kunne opfylde kravene til IG1.
    - Men det er meget usandsynligt, at de opnår de nødvendige krav til IG2.
3. Hvad kunne BS Consulting A/S gøre for at forbedre deres cybersikkerhedsmodenhed i forhold til deres risikoprofil og størrelse samt bevæge sig til en højere implementeringsgruppe?
    - Bevæge sig væk fra deres reaktive tilgang, over til en tilgang hvor de prøver at komme foran potentielle fejl og mangler i deres system.
    - Opdater deres sikkerhedspolitik regelmæssigt, samt at træne medarbejderne i den.