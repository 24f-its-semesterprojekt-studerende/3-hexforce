# Opgave 25.1 Opsætning af Wazuh-server

Wazuh opsættes på den server der blev sat op ugen før [Uge 9 Proxmox setup](uge_09_Proxmox.md)


Kør Wazuh installations assistentent med kommandøn under:  
`curl -sO https://packages.wazuh.com/4.7/wazuh-install.sh && sudo bash ./wazuh-install.sh -a -i`

Her har vi prøvet xterm.js terminal serial2 for at kunne kopiere kommandøn ind istedet for selv at skrive den ind.  
Hvis noget skal ændres i kommandøn så ændre den lokalt for derefter at paste en ind i serial terminalen. Ændring af en kommando som er blevet insat i serial terminalen kan være problematisk.  

Overblik over protokoller og porte som Wazuh bruger:  

|Komponent|Port|Protokol|Brug|
|---------|----|--------|-------|
|Wazuh Server|1514|TCP|Agent connection service|
|Wazuh Server|1514|UDP|Agent connection service (disabled by default)|
|Wazuh Server|1515|TCP|Agent enrollment service|
|Wazuh Server|1516|TCP|Wazuh cluster daemon|
|Wazuh Server|514|UDP|Wazuh Syslog collector (disabled by default)|
|Wazuh Server|514|TCP|Wazuh Syslog collector (disabled by default)|
|Wazuh Server|55000|TCP|Wazuh server RESTful API|
|Wazuh indexer|9200|TCP|Wazuh indexer RESTful API|
|Wazuh indexer|9300-9400|TCP|Wazuh indexer cluster communication|
|Wazuh dashboard|443|TCP|Wazuh web user interface|  


Herunder kan der ses hvilke processer der eksekveres i forbindelse med Wazuh, og hvilken bruger de eksekveres med.  
Det har vi brugt kommandøn 'ps aux | grep wazuh'.  
![Skærm Billede](images/opgave5_øvelse25.1.png)  
Vi kan se at Wazuh bruger brugere som root og wazuh selv til at eksekvere med.  

Det er ikke hensigstmæssigt at Wazuh bruger root idet vi har lukket ned for root brugerens password og vi derfor ikke kan tilgå den. Det virker lidt mærkeligt, hvorfor istallationen opretter en ny bruger.