# Opgave 25.2 Opsætning af Wazuh agent
opsætte en Wazuh-agent på den Ubuntu-instans, der ikke fungerer som vært for Wazuh-serveren, altså den Ubuntu-VM på Proxmox med navnet Target host.
## Deploying Wazuh agents on Linux endpoint

### add the wazuh repository
Install the GPG key:  
`curl -s https://packages.wazuh.com/key/GPG-KEY-WAZUH | sudo gpg --no-default-keyring --keyring gnupg-ring:/usr/share/keyrings/wazuh.gpg --import && sudo chmod 644 /usr/share/keyrings/wazuh.gpg`

`echo "deb [signed-by=/usr/share/keyrings/wazuh.gpg] https://packages.wazuh.com/4.x/apt/ stable main" | sudo tee -a /etc/apt/sources.list.d/wazuh.list`

`sudo apt-get update`

Herunder kan vi se at vi har udarbejdet et netværksdiagram for at kunne etablere netværksforbindelse, samt at få en bedre forståelse af 01MGMT, 02DMZ og 03MONITOR netværket.  
![Netværks diagram](images/øvelse_25.2/Netværksdiagram_øvelse_25.2.drawio.svg)

Target-Host er blevet tilføjet til 03MONITOR, og der er lavet en midlertidig firewall regel så Target-host har internet forbindelse til at downloade wazuh-agent. Reglen blev slukket lige efter.

`sudo WAZUH_MANAGER="10.33.30.20" apt-get install wazuh-agent`

### enable og start wazuh agent service
For at aktivere og starte Wazuh-agent, skulle vi benytte de nedenstående kommandoer:  
`sudo systemctl daemon-reload`  
`sudo systemctl enable wazuh-agent`  
`sudo systemctl start wazuh-agent`  

For at ændre WAZUH_MANAGER ip efter installation gør følgende som sudo:  
Ændre filen `/var/ossec/etc/ossec.conf`  
Ændre IP som set under:  
![ossec.conf skærmbillede](images/øvelse_25.2/image.png)

## Verificer forbindelse  
Herunder kan vi se at vi har forbindelse til Wazuh-agent.  
![Wazuh skærmbillede](images/øvelse_25.2/image-1.png)


## Opsætning og afprøving af Wazuh agent
Der blev oprettet en ny bruger "darth". Under Security events i wazuh kunne denne event ses.  
![Wazuh skærmbillede](images/øvelse_25.2/image-2.png)
Wazuh har set ændringen i loggen `/var/log/auth.log`