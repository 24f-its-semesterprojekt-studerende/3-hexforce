# Øvelse 53

Her kan du finde vores besvaringer til øvelse 53.

## Rasmus:

![Rasmus](../netværk/images/øvelse_53/øvelse53Rasmus.png)

### Redegør for dine valg omkring segmentering og firewall regler. Det vil sige at andre skal kunne forstå hvad du har valgt at gøre og hvorfor du har valgt at gøre det.
Giver ikke nogen mening at have VPLE der kan kommuniker udaf mod internettet, da det er brugt som et testbed, så at holde det aflukket er en god ide.
Logging serveren får bare logs fra OPNsense, så kan heller ikke se hvorfor den skulle kunne tilgå resten af internettet.
Webserveren leverer en webserver, som jeg går udfra er meningen at andre folk, der ikke er på netværket, skal kunne tilgå, derfor får den adgang til at kunne tilgås.
Kali er bare brugt til at teste netværket, derfor er den sat til at kunne tilgå alt andet på netværket, men ikke andet, da den ikke er brugt til det.

## Mikkel:
Der skal designes et netværk der understøtter følgende:

- 1 OPNsense router
- 1 webserver der server en intranet side via https
- 1 server vm der opsamler netværkslog fra OPNsense. Her skal [graylog](https://github.com/Graylog2/docker-compose/blob/main/open-core/docker-compose.yml) i docker compose bruges.
- 1 server vm der kører usikre services i [VPLE](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/)
- 1 kali Linux vm der der benyttes til at teste netværket


### Porte
Tabellen under viser de porte de forskellige enheder kræver

|Service|porte|
|---|---|
|Webserver|443|
|Graylog|8999, 9200, 9300, 5044, 5140, 5555, 9000, 12201, 13301, 13302|
|VPLE|1335, 1336, 1337, 8080, 3000, 8899, 8800|

### Netværksdiagram
![Netværks diagram](images/øvelse_53/netværksdiagram_mikkel.drawio.svg){ width=600 }

### Firewall regler
Firewall allow regler over porte

|Fra↓/Til→   | Subnet 1 | Subnet 2 | Subnet 3 | Internet |
|------------|----------|----------|----------|----------|
|**Subnet 1**| X        | 443      | VPLE**   |Allow all |
|**Subnet 2**| Deny all | X        | Deny all | Deny all |
|**Subnet 3**| Deny all | Deny all | X        | Deny all |
|**Internet**| Deny all | Deny all | Deny all | X        |


** Porte som VPLE bruger, se i tabellen over.

Udover det ovennævnte skal firewallen fra Subnet 1 til OPNsense tillade porte der bruges til Graylog.

### Begrundelse af valg
#### Segmentering
VLPE og Webserveren skal ikke kommunikere med hinanden og er derfor segmenteret for sig for at undgå "lateral movement".

Graylog og Kali Linux er på samme segment. Dette er gjort da netværksadministratoren under en test af netværket vil skulle tilgå logs ofte. Selvom det muligvist øger sikkerheden, at hver enhed er segmenteret hver for sig, vil det hurtigt blive upraktisk at lave lige så mange segmenter som enheder. 
#### Firewall regler
Kali Linux skal kunne bruge nogle specifikke services to VLPE og webserveren. Disse specifikke porte der derfor åbne fra subnet 1 til de andre subnets OPNsense.

Fordi OPNsense er statefull er det ikke nødvendigt at have åbne porte fra internettet, Subnet 2 og 3 ind til Subnet 1.

Firewall på Subnet 1 og 2 er sat til "deny all" fordi disse enheder ikke selv der starter kommunikationen med andre enheder. 

## Tobias
Herunder ses mit bud på netværksdiagrammet.
Jeg har brygget det op så OPNsense har fem interfaces med tilknyttet; Netværkslogs, Webserver, en usikker VLPE server og Kali Linux.  

### Firewall og porte
Netværkslog: Skal kunne tillade logs trafik fra OPNsense.  
Webserver: Skal tillade indgående trafik på porte 80(HTTP) og 443(HTTPS) og blokere alt andet.  
Kali Linux: Skal tillade at sende trafik ud (Allow all) og skal forhindre indgående uautoriseret adgang (deny all).  
VLPE: Tillader usikre porte.  

![Netværks diagram Tobias](images/øvelse_53/øvelse_53_Tobias.drawio.svg)