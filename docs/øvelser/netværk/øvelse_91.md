# Øvelse 91 - Grundlæggende ARP



- Find ARP requests
 - Forklar hvad der ses
 - ![Wireshark ARP Broadcast](images/øvelse_91/image-7.png)

- Ping en ikke-eksisterende ip adresse og find arp requests
 - Forklar hvad der ses
 - ![Arp requests](images/øvelse_91/image-5.png) 


- Kør `arp -e`
 - Forklar hvad der ses
 - ![Arp table](images/øvelse_91/image-6.png)

- Opsamling på klassen - en gruppe præsenterer
