# Øvelse 100 - Trådløs viden

1. Hvad er 802.11? Forklar med egene ord.
    - 802.11 er en samling af standarder omkring WLAN. Det er den første standard i IEEE standarder.
2. Hvor mange standarder findes der pt. i 802.11 familien?
    - Der findes 21 standarder. 
3. Hvad er Wi-Fi?
    - Netværks protokoler som er baseret på 802.11 standarder.
4. Hvilke frekvenser benyttes?
    - 2.4 GHz, 5 GHz, 6 GHz (For Wi-Fi 7), 42.5 GHz og 71 GHz (kommer i Wi-Fi 8)
5. Hvad er kanaler?
    - Hver frekvens er delt op i flere kanaler fordelt med 5 MHz mellem hver kanal. Selvom kanalen kun er 5 MHz bred, så optager en transmission mindst 20 MHz.
    - Kanaler har specifikke frekvenser.
        - For 2.4 GHz går det fra 2401 MHz til 2495 MHz.
            - Hver kanal er 22 MHz bred.
        - For 5 GHz går det fra 5150 MHz til 5895 MHz.
            - Hver kanal er 20 MHz bred.
    - Kanal frekvenser kan varier fra land til land, baseret på lokale love.
6. Hvor mange MB svarer 1000 Mb til? (hvad er formlen til at omregne?)
    - 125 kB
    - $ MB = Mb\cdot \frac{1}{8}$ eller $ MB = Mb\cdot 0.125$
