# Øvelse 84 - Graylog konfiguration
## Konfigurer OPNsense
### Send logs til Graylog
Konfigurer OPNsense til at sende logs til Graylog
Gå til `System -> Settings -> Logging/targets` og tryk `+`.
Fonfigurer destination som set på skærmbilledet under:    
![OPNsense log destination opsætning](images/øvelse_84/image.png)

### Send Netflow Data til Graylog  
Konfigurer destinationen for Netflow Data

Gå til `Reporting -> Newflow` og tilføj Graylog til `Destinations`, som vist på skærmbilledet under.  
![OPNsense NetFlow destination opsætning](images/øvelse_84/image-1.png)

## inputs

Gå til `System / Inputs -> Inputs` vælg `Syslog UDP` og tryk `Launch new input`  
![Alt text](images/øvelse_84/image-3.png)

Udfyld titel og port.  
![Alt text](images/øvelse_84/image-4.png)

For Firewall log input skal `Store full message` været slået til.

Gør det igen, men vælg denne gang `NetFlow UDP` og port 2055.


## indeks
Opret et indeks til Syslog.   
Gå til `System->Indices` og klik på `Create Index Set`.  
![Alt text](images/øvelse_84/image-5.png)

Udfyld `Titel`, `Description` og `Index Prefix`. Konfigurer `Index Rotation Configuration`, `Index Retention Configuration`

Bekræft og lav et indeks til NetFlow.

## Opret stream 
Gå til `Streams` tryk `Create Stream` udfyld `Title` og sæt `Index Set` til et af de indeks der blev oprettes tidligere.  
![Alt text](images/øvelse_84/image-8.png)  
Opret en stream mere med det andet indeks.

Gå til `System / Inputs -> Inputs`, og tryk `Show received messages`. Vær opmærksom på hvilket input det er.
![Alt text](images/øvelse_84/image-7.png)
Kopier strengen som ses på billedet under.  
![Alt text](images/øvelse_84/image-6.png)

Gå tilbage til `Streams`. For det index strengen blev kopiret fra, tryk `More` og `Manage Rules`. Tryk `Add stream rule`. Udfyld `Field` og `Value`. `Field` er den streng der så før `:` i den kopirede streng og `Value` strengen efter `:`.

Gør det samme igen for det andet index.

## Extractors
Firewall trafiken vil blive vist som en langt streng, som set på skærmbilledet under. I denne streng er det ikke muligt at filtrerer.
![Alt text](images/øvelse_84/image-9.png)

For at gøre filtrering mulig skal der opsættes nogle extractors.  
Gå til `System / Inputs -> Inputs`, for firewall logs vælg `Manage extractors`.  
![Alt text](images/øvelse_84/image-11.png)  
Klik på `Actions -> Import extractors`

Kopier indholdet fra [denne json fil](https://github.com/IRQ10/Graylog-OPNsense_Extractors/blob/master/Graylog-OPNsense_Extractors_RFC5424.json) og tryk `Add extractors to input`.

Det skulle nu være mulig af filtrerer firewall logs.

## Graylog Search
Når der bliver lavet en scanning med `ffuf`, viser det sig tydeligt, som set på skærmbilledet under.
![Alt text](images/øvelse_84/image-13.png)
Det samme gælder for en `nmap` scanning. Skærmbilledet under, viser de gange firewall har blokeret trafik.
![Alt text](images/øvelse_84/image-12.png)