- Lav en brainstorm i jeres gruppe over hvordan i kan bruge red teaming i jeres semester projekt? Her er lidt inspiration til at komme i gang med snakken.

    - Kig i Mitre Att&ck: Hvilke taktikker og teknikker kan i afprøve på jeres proxmox installation
    - Hvordan kan i beskrive en red team øvelse i jeres eksamensrapport?
    - Er det aktuelt for jer at bruge for eksempel Unified kill chain og i så fald hvordan?


Rekonstruerer et angreb der er sket.
- Sæt et netværk op som det var.
- og prøv at angrebe det.

Gem password på et segment, så der kan opnås adgang til et andet segment.

Blue Team
Segmenterer , OPNsense Firewall, Suricata IPS/IDS, SIEM Wazuh, Greylog monitorering af logs, 
opsæt VPN og åben ssh. Brug ssh til forbindelse til (alle?) andre enheder.

Lav et brute force attack på SSH

Brug ikke password til SSH meb i stedet brug kun keys.

DMZ, http webserver Port forwarding?

MODsecurity til Nginx og apache til hardening.