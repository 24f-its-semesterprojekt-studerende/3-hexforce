# Øvelse 41 - Nmap wireshark

[Link til Øvelse 41](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/41_nmap_wireshark/)

4  `nmap -sC -v -oA nmap_scan 192.168.1.1`

5  Med kommandøn over skanners OPNsense routeren

6 Nmap scannet viser at port 53, 80, 443 er åbne.

7  Protokollerne der kører på disse porte er hhv. DNS, HTTP, HTTPS

8 Sådan ser traffikken ud, boksen markerer forskellige porte som bliver skannet. ![alt text](images/øvelse_41/image.png)

10 En webserver startes med `sudo python3 -m http.server 8000`

11 I en browser der webserver sådan ud:  
![alt text](images/øvelse_41/image-1.png){ width=300 }

12 Følgende kommando `nmap -sC -v -oA nmap_scan_webserver 192.168.1.103` giver følgende resultat   
![alt text](images/øvelse_41/image-2.png){ width=400 }

13 I 12. skanner vi Kali maskinen

14 Skannet viser at port 8000 er åben (Den røde boks).

15 Skannet viser også at den service er kører at http-alt

16 ![alt text](images/øvelse_41/image-3.png)  

19 Der er ingen services da webserveren ikke kører. 