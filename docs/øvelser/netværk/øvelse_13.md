# Øvelse 13 - Protokolforståelse

Kig på data som du sniffede i Øvelse 12
## TCP
find et TCP handshake
![alt text](images/øvelse 13/image.png)
192... sense en SYN til 34... 

34... senser SYN, ACK tilbage

192... svarer med ACK,

og det svare til det det re beskrevet i [RFC9293 afsnit 3.5](https://www.rfc-editor.org/rfc/rfc9293.pdf)


TCP header er bygget som set på billedet under  
![alt text](images/øvelse 13/image-1.png)  
[RFC9293](https://www.rfc-editor.org/rfc/rfc9293.pdf)


Hvor vi kigger på wireshark kan vi se src port, dst port, seq, len, som scare til Source Port, Destination Port, Sequence Number, Acknowledgment Number fra RFC9293.  
![alt text](images/øvelse 13/image-2.png)

## TLS

### Klient
Under vises et TLS handshake
![alt text](images/øvelse 13/image-3.png)

Det ses at klienten udderstøtter 17 Cipher Suites.

scoller man lidt længere ned ses det at klienten understøtter TLS 1.2 og 1.3  
![alt text](images/øvelse 13/image-4.png)


### Server
#### Version
Under protocol står det TLSv1.3, men under handshake protocol står det TLS 1.2??  
![alt text](images/øvelse 13/image-6.png)
#### Cipher Suite
Serveren vælger Cipher Suite en ud af de 17.  
![alt text](images/øvelse 13/image-5.png){ width="400" }