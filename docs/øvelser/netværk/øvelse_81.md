# Øvelse 81 - Portainer på Docker Host

Vi har fuldt [denne guide](https://docs.portainer.io/start/install-ce/server/docker/linux) for at installere Portiner til vores Docker enhed på 03MONITOR netværkssegmentet.

Herunder ses Firewall reglen for 01MGMT segmentet:  
![alt text](images/øvelse_81/image-1.png)

For at tilgå Portainer fra vores Kali søger vi Portainer'ens IP adresse som er `10.33.30.10` med portnummeret `9443`. Herunder ses vores Portainer webside:  
![alt text](images/øvelse_81/image-2.png)