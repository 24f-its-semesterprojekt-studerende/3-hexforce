# Øvelse 54 Implementering af netværksdesign

Dette netværk skal sættes op i proxmox.  
Netværket er sat up ved at følge denne [guide](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/54_network_design_implementation/).

![alt text](images/øvelse_54/lab_diagram_20240301.png)  
*Lab diagram 20240301 - NISI*

IP adressernes anden oktet, i billedet vist som XX, skal være 33.
## OPNsense opsætning
Fire interfaces er blevet sat op. 

Interface|IP
--|--
00WAN | DHCP  
01MGMT | 10.33.10.1  
02DMZ | 10.33.20.1  
03MONITOR | 10.33.30.1  

Efter en reboot af OPNsense, kunne disse interfaces ses i OPNsense CLI.  
![alt text](images/øvelse_54/image-12.png)  
Skærmbilledet viser at de fire interfaces er sat rigtigt op.

## Kali opsætning
En statisk IP er skat op på Kali.  
Skærmbilledet viser at Kali kan tilgå OPNsense webinterface.  
![alt text](images/øvelse_54/image-9.png)
### nmap scan

Der laves et nmap scan fra 01MGMT TIL 02DMZ med kommandøn `sudo nmap -sC 10.33.20.1`    
![alt text](images/øvelse_54/image-10.png)
Billedet viser at port 53, 80 og 443 er åbne

Der laves et nmap scan fra 01MGMT til 03MONITOR med kommandøn `sudo nmap -sC 10.33.30.1`    
![alt text](images/øvelse_54/image-11.png)
nmap scannet viser at port 53, 80 og 443 er åbne

Der laves et nmap scan fra 02DMZ til 01MGMT med kommandøn `sudo nmap -sC 10.33.10.1`    
og det samme fra 02DMZ til 03MONITOR (`sudo nmap -sC 10.33.30.1`)
![alt text](images/øvelse_54/image-13.png)
Begge resultater viser at port 53, 80 og 443 er åbne.

Fra 03MONITOR  
Tjekker først om der er forbindelse til interface med `ping 10.33.30.1`  
Med et nmap scan fra 03MONITOR til 01MGMT (`nmap -sC 10.33.10.1`) der er port 53, 80, 443 åbne  
Med et nmap scan fra 03MONITOR til 02DMZ (`nmap -sC 10.33.20.1`) der er port 53, 80, 443 åbne  

## Alias
Alias der er blevet opsat, under `Firewall -> Aliases`:

Name: PrivateNetworks  
Type: Networks  
Content: 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16  

Name: VPLE_PORTS  
Type: Ports  
Content: 80, 1335, 1336, 3000, 8080, 8800, 8899  

## Firewall opsætning
Disse regler er blevet tilføjet til hhv. 01MGMT, 02DMZ og 03MONITOR:  
Firewall regler 01MGMT  
![alt text](images/øvelse_54/image-14.png)

Firewall regler 02DMZ  
![alt text](images/øvelse_54/image-15.png)

Firewall regler 03MONITOR  
![alt text](images/øvelse_54/image-16.png)

Efter et nmap scan fra 01MGMT til 02DMZ ses det at ingen porte er åbne. Dette viser at firewall reglerne er sat op korrekt.
Det samme gøres fra 01MGMT til 03MONITOR, med samme resultat.

Fra 02DMZ til 01MGMT, er kun port 53 åbne. Præcis som forventet.

Når VLPE skannes (se skærmbillede under) ses det er port 80, 1335, 1336, 1337, 3000, 8080, 8800 og 8899. Dette er som forventet da disse er sat op i firewall under VPLE alias.  
![alt text](images/øvelse_54/image.png)

## VPLE
Et VMware .ova fil til VPLE skal tilføjes til Proxmox. Dette blev gjort ved at følge denne [guide](https://jalokin.com/proxmox/proxmox_convert_ovf/#forums).
Hver opmærksom på punk 5. når man skal extract VPLE.ova til .ovf **i /temp directory**

Under punkt 6 Husk at ændre kommandøns ID (`<ID>`) `qm importovf <ID> /tmp/exported-vm.ovf local-lvm` til det rigitge ID, f.eks. til 305 som vi har gjort, det du skal bruge.


Fejlfinding:
Der var et problem med at enheden ikke kunne få en IP adresse tildelt over DHCP.  
Vi tror problemet lå i at DHCP fra 01MGMT ikke har sat korrekt op. 01MGMT havde en IP range fra et tidligere adresser. Denne range blev slettet og så virkede det?

Billedet under viser at DHCP kører og har tildelt en adresse til VPLE maksinen og at Kali maskinen kan pinge den.  
![alt text](images/øvelse_54/image-17.png)

## Begræns OPNsense GUI adgang
Her har vi opsat det sådan at det kun er 01MGMT, der kan tilgå OPNsense WEB UI fra Kali Linux, hvor de andre systemer 02DMZ og 03MONITOR ikke har mulighed for at tilgå.

Før var den empty for at alle kunne tilgå interfaced.

Under Secure Shell, har vi også her tilføjet 01MGMT til Listen Interfaces, for at det kun er 01MGMT, som kan benytte SSH for OPNsense.

Det blev test ved at flytte Kali Linux til de forskellige subnets. Her var det kun fra 01MGMT, at OPNsense webinterface kunne tilgås. Fra 02DMZ og 03MONITOR blev det blokeret.02