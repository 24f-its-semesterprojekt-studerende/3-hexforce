# Øvelse 103 - trådløs CTF
Nu er det tid til at i skal prøve kræfter med at bryde ind i min router. Jeg har derfor lavet en lille CTF som har 2 mål-

1. Find netværkets PSK så i kan logge på routerens netværk.
2. Find routerens password så i kan ændre SSID på 5GHz netværket


Der er nogle regler som i skal følge:

1. HUSK AT I IKKE MÅ RØRE VED ANDET END MIN ROUTER, I ER SELV ANSVARLIGE FOR AT OVERHOLDE LOVEN!!
2. Jeg er CTF dommer og afgør alle tvivl, tvister og spørgsmål!
3. Hurtigeste tid vinder!
4. I er i mål når i har ændret ssid på 5GHz netværket (SSID: Z-5) dokumenteret på jeres gitlab og sendt link til dokumentationen på min mail adresse.
5. Craches routeren lægges der 30 minutter til tiden!
6. Ændres der med overlæg pw til management eller 2.4GHz eller andet der ødelægger det for andre, er gruppen diskvalificeret og skal gi kage til hele klassen nøste undervisningsgang!
7. Bedste dokumentation, den med flest detaljer om netværket og anvendte metoder får trukket 30 minutter fra sin tid!

Vi bruger Airgeddon til at finde PMK not routeren z-24
Vi benytter os af denne guide: https://www.hackingarticles.in/wireless-penetration-testing-airgeddon/

Først har vi fået Airgeddon til at scanne efter PMKID

Så vi starter airgeddon i command prompt med `airgeddon` hvor vi derefter kun kan vælge 1. 

## Inden vi begynder:
Før vi begynder vores angreb skal vi først sætte arigeddon i monitor mode ved at klikke på menu 2. Put interface in monitor mode.

Herefter kan vi starte angrebet.

## Angrebet
Derefter har valgt 5. Handshake/PMK tools menu. Så har vi valgt capture handshake og scanner for alle netværk. Vælger Z-24.

Derefter bruger deauth aireplay attack hvor vi har valgt en timeout på 50 sekunder, hvilket er vigtigt i forhold til at den default tid på 25 sekunder, får den til at faile. Vi skulle derefter have et handshake.

Herunder ses vores capture af et handshake efter 50 sekunders timeout.  
![handshake capture](images/øvelse_103/image.png)

Herefter går vi til Main menu -> 6. Offline WPA/WPA2 decrypt menu -> her vælger vi 1 Personal -> Herefter vælger vi blot den første menu 1. (aircrack) Dictionary attack against Handshake/PMKID capture file.

Herefter vælger vi at bruge et dictionary attack ved brug af rockyou.txt. 
Herunder kan der ses et udsnit af hvordan rockyou.txt filen ser ud når den Bruteforcer. Man ser kun et glimt af en tekst filen før den skifter igen.

![Brute force](images/øvelse_103/image-1.png)
 
Vi finder at kodeordet er spongebob1!  
![Kodeord](images/øvelse_103/image-2.png)

Vi logger på Z-24 med kodeordet.

Gå til deafault gateway på 192.168.50.1

Routernavnet er Zyndicate-rocks-you 

Zyndicate er en hackergruppe der hackede netcompany. Det viste at netcompany brugte kodeordet "Netompany123".


Logger på wireless setting med.
user: root
pass: Netcompany123

Ændre SSID til HexForce  
![Ændre SSID](images/øvelse_103/image-4.png)
Det kan ses i Windows  
![Hexforce SSID](images/øvelse_103/image-3.png)