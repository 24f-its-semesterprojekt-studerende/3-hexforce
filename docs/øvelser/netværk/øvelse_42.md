# Øvelse 42 - Enumering med offensive værktøjer

Instruktioner og svar for øvelsen.

1. Installer seclists med `sudo apt install seclists`
    - Vi kører installationen på Kali i proxmox.  

2. Kør en ffuf skanning mod VPLE maskinen og DVWA servicen med `ffuf -r -u <VPLE_IP>:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt`.  
    - Her efter kører vi kommandoen `ffuf -r -u http://10.33.20.100:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt`.  

3. Snak i jeres gruppe om hvad i ser. Hvilke http status koder får i, hvad betyder de og hvilke er i mest interesserede i? Diskuter hvor i jeres system i kan monitorere at der laves wordlist skanninger? opnsense måske eller ??
    - Vi får mange statuskoder 200 og 403, hvor statuskode 200 er OK og 403 er permission denied.  
    ![Billedet over hvad vi ser](images/øvelse_42/image.png)  
    - Her er vi mest interesseret i statuskoden 200 OK, fordi de er åbne.  

4. Det er muligt at filtrere ffuf's output med specifikke status koder med parametret -mc efter fulgt af statuskoderne f.eks. 200, 302. Det er også muligt at filtrere på størrelse med -fs efterfulgt af den størrelse svar i ikke vil have med i outputtet.  

    - For kun at få statuskoden 200 vist med ffuf skriver vi `ffuf -r -u http://10.33.20.100:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -mc 200`.  
    ![Billede af -mc 200 statuskode](images/øvelse_42/image-3.png)  

    - Ved -fs kigger man på de filtrerede fra indenfor en range på 200-400  

5. Lav en ny scanning og filtrer outputtet så i kun ser svar med http respons 200. Kig i netflow trafikken på opnsense samtidig med at i laver skanningen. Kan i se der at den udføres?
    `ffuf -r -u <VPLE_IP>:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -mc 200`.  
    - Det ser således ud:
    ![Billede af 200 statuskoden samt traffic fra Proxmox](images/øvelse_42/image-1.png)

6. Snak i gruppen om de svar i ser nu. giver det et bedre overblik og ville det være bedre at bruge -fs parametret?
    - Vi får de samme svar og man skal vide hvilke porte, man sortere fra, hvilket godt kan være problematisk, hvis det ikke er noget man ved.

7. Prøv at lave ffuf skanningen med en anden wordlist fra seclists/Discovery/Web-Content mappen. Får i andre resultater?  
    - Vi vælger `ffuf -r -u http://10.33.20.100:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/dirsearch.txt -mc 200`, som kan ses på billedet herunder.  
![filter med dirsearch.txt](images/øvelse_42/image-4.png)

8. Lav en ny skanning på en af de fundne url's for eksempel: `<VPLE_IP>:1335/docs/FUZZ`, `<VPLE_IP>:1335/external/FUZZ` eller   `<VPLE_IP>:1335/config/FUZZ`. Hvilke resultater får i og kan i genfinde skannnigerne i nogle logs på jeres system?
    - Vi får ingen nye resultater.

Vi prøver at lave de yderligere scanning for /docs, /external og /config. Det gør vi ved hjælp af disse kommandoer:  
/docs: `ffuf -r -u http://10.33.20.100:1335/docs/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/dirsearch.txt -mc 200`  
![/docs/FUZZ](images/øvelse_42/image-7.png)
/external: `ffuf -r -u http://10.33.20.100:1335/external/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/dirsearch.txt -mc 200`  
![/external/FUZZ](images/øvelse_42/image-5.png)  
/config: `ffuf -r -u http://10.33.20.100:1335/config/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/dirsearch.txt -mc 200`  
![/config/FUZZ](images/øvelse_42/image-6.png)