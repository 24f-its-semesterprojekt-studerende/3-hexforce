# Øvelse 51 - Netværksdesign med netværkssegmentering

1. I jeres gruppe skal i undersøge og diskutere, hvilke segmenter der bør være i netværket. Overvej faktorer som afdelingsopdeling, sikkerhedszoner, usikre enheder og behovet for at beskytte følsomme data.

Afdelingerne er:

Teknologi og IT:

- Netværksinfrastruktur: Ansvar for styring og administration af routere, switches og andre netværksenheder.
    - Seperat netværk der er koblet til alt infrastruktur - lukket af fra andre. Ikke kunne tilgås udefra.
- On-premise servere: Hosting af applikationer, databaser og andre tjenester.
    - De services der er rettet udaf skal have adgang til internettet, mens de der er rettet til at bruges internt lukkes af, og kun kobles til de netværk der har brug for dem.

Ledelse og Administration:

- Topledelse: Strategiske planer, øverste ledelsesbeslutninger og følsomme interne oplysninger.
    - Adgang til internettet, lukket af fra andre netværk, koblet til de databaser (hvis nogen) de bruger.
- HR (Human Resources): Indeholder personlige oplysninger, ansættelseskontrakter, løn og andre følsomme HR-data.
    - Adgang til internettet, forbindelse til finans i.fht. lønninger, koblet til de databaser (hvis nogen) de bruger.
- Finans: Inkluderer økonomiske oplysninger, regnskaber og lønningsoplysninger.
    - Adgang til internettet, forbindelse til HR i.fht. lønninger, koblet til de databaser (hvis nogen) de bruger.

Operationelle Afdelinger:

- Produktion: Produktionsdata og styring af produktionslinjer.
    - Ingen adgang til internettet. Adgang til lager netværk. Lukket fra alle andre.
- Lager: Opbevaring af varer og lagerstyring.
    - Ingen adgang til internettet. Giver adgang til produktion. Lukket fra alle andre netværk. Serperat netværk der bruges til at kommunikerer med leverandører.
- Leverandørkæde: Data relateret til leverandører og forsyningskædeprocesser.
    - Adgang til lager for processen af at kunne købe de ting hjem der er behov for. Kun adgang til et seperat netværk der ikke kan tilgå produktion.

