# Øvelse 2 - Grundlæggende netværksviden

1. Hvad betyder LAN?
    - Local Area Network
2. Hvad betyder WAN? 
    - Wide Area Network
3. Hvor mange bits er en ipv4 adresse?
    - 32 bits
4. Hvor mange forskellige ipv4 adresser findes der?
    - 4,294,967,296 (2^32)
5. Hvor mange bits er en ipv6 adresse?
    - 340,282,366,920,938,463,463,374,607,431,768,211,456 (2^128)
6. Hvad er en subnet maske?
    - Hvor bredt netværket, segmentationen af netværket
7. Hvor mange hosts kan der være på netværket 10.10.10.0/24
    - 254
8. Hvor mange hosts kan der være på netværket 10.10.10.0/22
    - 1022
9. Hvor mange hosts kan der være på netværket 10.10.10.0/30
    - 2
10. Hvad er en MAC adresse?
    - Media Access Control
11. Hvor mange bits er en MAC adresse?
    -  48 bits
12. Hvilken MAC adresse har din computers NIC?
    -  D8-12-65-76-AE-4B (Mikkels)
13. Hvor mange lag har OSI modellen ?
    -  7
14. Hvilket lag i OSI modellen hører en netværkshub til?
    - Fysiske lag
15. Hvilket lag i OSI modellen hører en switch til?
    -  Data link
16. Hvilket lag i OSI modellen hører en router til? 
    - Network
17. Hvilken addressering anvender en switch?
    - MAC
18. Hvilken addressering anvender en router?
    - IP addresser
19. På hvilket lag i OSI modellen hører protokollerne TCP og UDP til?
    -  Transport
20. Hvad udveksles i starten af en TCP forbindelse?
    - 3 Way Handshake
20. Hvilken port er standard for SSH?
    - 22
21. Hvilken port er standard for https?
    -  443
22. Hvilken protokol hører port 53 til?
    -  DNS
23. Hvilken port kommunikerer OpenVPN på? 
    - TCP: 443, UDP: 1194
23. Er FTP krypteret? 
    - Ikke uden addon (FTPS)
24. Hvad gør en DHCP server/service? 
    - Uddeler IP adresser til hosts på netværket.
    - Dynamic Host Configuration Protocol
25. Hvad gør DNS? 
    - Oversætter domæner til IP adresser (vice versa)
    - Domain Name System
26. Hvad gør NAT?
    -  Network Address Translation
    - Oversætter IP adresser fra et netværk til et andet
27. Hvad er en VPN? 
    - Virtual Private Network
    - Gør at man tilkoble et netværk uden at være fysisk tilkoblet
28. Hvilke frekvenser er standard i WIFI? 
    - 2.4 GHz, 5 GHz
29. Hvad gør en netværksfirewall ? 
    - Analyser trafik, og frasorterer ondsindet trafik baseret på configuration.
30. Hvad er OPNsense?
    - Firewall og routing software.

