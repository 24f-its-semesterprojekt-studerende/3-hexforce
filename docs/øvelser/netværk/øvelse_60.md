# Øvelse 60 Viden om firewalls

Lav en kort beskrivelse af hvad de forskellige typer kan, hvilket lag i OSI modellen de arbejder på og hvad deres svagheder er (hvordan de kan kompromitteres ved f.eks ip spoofing) 

- Pakkefiltreringsfirewall
    - Pros: Filtrere indgående og udgående pakker. Hurtigt at filtrere på lag 3. Gode mod DDOS-angreb.
    - Cons: Kan ikke se på application-layer data. Sårbar over for IP-spoofing.
    - Layer: 3 og 4
- Stateful Inspection Firewall
    - Pros: Dynamisk hukommelse der laver en tabel af indgående og oprettet forbindelser. Har basale regler for blockering af packets.
    - Cons: Mere complex end pakkefilterering, hviklet medfører formindsket netværk performance.
    - Layer: 4
- Application Layer Firewall (Proxy Firewalls)
    - Pros: Sat mellem en remote bruger og en server, masker begge parter så de kun ser firewallen. God for sikkerhed mellem private og public netværk. God til at beskytte applikationer. Støtter ting som biometric og passwords. Den kan filtre specifikke packets fx. .exe filer.
    - Cons: Den høje sikkerhed medfører dårligere hastighed og høje omkostninger pga. databehandling på applikationsniveauet.
    - Layer: 7
- Next-Generation Firewall (NGFW)
    - Pros: Indeholde flere funktioner fra mange foregående Firewall, hvor den dermed kan blokere malware inden den når ind til infrastrukturen.
        - Cons: Koster mere end andre typer. Skal integreres på en dybere måde i sikkerhedssystemer end andre firewalls, hvilket kan være en meget kompleks process.
    - Layer: Layer 2 - 7
- Proxy Server Firewall (f.eks Web Application Firewall)
    - Pros: Kan analyser data fra understøttet protokoller på en mere detaljeret måde end andre firewalls. Cacher indkommende traffik. Set som den mest sikre type firewall.
    - Cons: Denne firewall er også en dyr løsning for sikkerheden. Single point of failure. Formindsker netværks hastighed. Kan være kompliceret at sætte op. 
    - Layer: 7