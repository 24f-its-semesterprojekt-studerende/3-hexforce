# Øvelse 80 - Virtuel Docker host

Inden vi har gået i gang med opgaverne 80-84, hvar vi først ændret vores [netværksdiagram](images/øvelse%2080/Netværksdiagram.drawio.svg).  

Vi har først tilføjet en ny VM, som hedder Docker, den har følgende specs:
![Docker Hardware](images/øvelse_80/image.png)  

MAC Adressen af Docker VM er BC:24:11:CF:E9:AD, hvor vi under OPNsense i ISC DHCPv4->03MINOTOR-> tilføjet Dockeren til en statisk DHCP.

## Installering af Docker på Docker VM
### Inden vi kan downloade Docker
Først opdaterer vi vores liste af pakker: `sudo apt update`

Dernæst installere vi nogle forudsætninger for at apt kan bruge pakker over HTTPS: `sudo apt install apt-transport-https ca-certificates curl software-properties-common`

Så tilføjer vi GDP KEY: `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

Herefter udfører vi kommandoen `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"` som tilføjer Dockerens repository til APT sources.