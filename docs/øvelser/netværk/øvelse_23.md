# OPNsense Viden
1. Hvilke features tilbyder opnsense?
    - [Feature set](https://docs.opnsense.org/intro.html#feature-set)
2. Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder?
    - System -> Firmware -> Status  
    - ![alt text](images/øvelse_23/image.png)
3. Hvad er lobby og hvad kan man gøre derfra?
    - ![alt text](images/øvelse_23/image-1.png)
4. Kan man se netflow data direkte i opnsense og i så fald hvor kan man se det?
    - [Netflow basics](https://docs.opnsense.org/manual/netflow.html#netflow-basics)
    - reporting -> insight (kræver at det er sat op i netflow)  
    ![alt text](images/øvelse_23/image-2.png)
5. Hvad kan man se omkring trafik i Reporting?
    - gå til reporting -> traffic 
    - ingående og udgående data hastighed i bps
6. Hvor oprettes vlan's og hvilken IEEE standard anvendes?
    - Interfaces > other types > VLAN
    - IEEE 802.1Q
7. Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?
    - Der er sat to regler op for LAN og ingen for WAN
    - Det er 25 autogenereret regler.
    - ![alt text](images/øvelse_23/image-3.png)
8. Hvilke slags VPN understøtter opnsense?
    - IPsec, OpenVPN, Wireguard
9. Hvilket IDS indeholder opnsense?
    - Suricata [source](https://docs.opnsense.org/manual/ips.html)
10. Hvor kan man installere os-theme-cicada i opnsense?
    -   System > Firmware > plugins > søg os-theme-cicada
    - ![alt text](images/øvelse_23/image-4.png)
    - System > setting > general > vælg theme > save
11. Hvordan opdaterer man firmware på opnsense?
    - System > firmware > updates > status > check for updates > scroll til bunden > tryk update
12. Hvad er nyeste version af opnsense?
    - 24.1.1
13. Er jeres opnsense nyeste version? Hvis ikke så opdater den til nyeste version :-)
    - Den er opdateret.
