# Øvelse 25 OPNsense lokal overvågning


## DNS ovevågning
DNS aktiviteten på OPNsense kan overvåges fordi OPNsense bruger den lokale DNS server "Unbound".

Reporting -> Setting -> Unbound DNS reporting

![alt text](images/øvelse_25/image.png)

Så kan man åbne Reporting -> Unbound DNS

![alt text](images/øvelse_25/image-1.png)

Efter der bliver lavet en DNS request vil det vise antal og domæner som set under.

![alt text](https://i.imgur.com/a8T3Ch3.png)

## Opsætning af lokalt netflow
Netflow giver informationer om source og distination IP og porte mm.

Måden hvorpå vi sætter Netflow op på OPNsense er under Reporting --> netflow.  
Efter vi har navigeret til Netflow siden kan vi se dette:  

![alt text](images/øvelse_25/image-3.png)  

Herover ses efter vi har opsat Netflow op. Det var autogeneret med v9 Netflow version.


![alt text](images/øvelse_25/image-4.png)


![alt text](images/øvelse_25/image-5.png)

Der er forskellige slags side man kan tilgå over Pie Charts under Insight siden. Her kan man vælge LAN og WAN.  

## Firewall Live View
Her kan man se, hvad firewallen allow og rejecter. For at komme til Live View tilgår man Firewall->Log Files->Live View.  

![alt text](images/øvelse_25/image-6.png)

Der kan opsættes filtre efter behov. fx specifik ip adresse, port osv.

Herunder ses f.eks. vores filtrering af Live View med Kali Linux IP-adresse.  
![alt text](images/øvelse_25/image-7.png)

Alternative måde at se trafik der er blokeret 

![alt text](images/øvelse_25/image-8.png)
