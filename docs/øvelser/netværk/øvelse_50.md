# øvelse 50 Tegne netværksdiagram
Under ses netværksdiagrammer tegnet i draw.io med VScode extension.
Dette billede skulles tegnes ![diagram skitse](https://ucl-pba-its.gitlab.io/24f-its-nwsec/images/fysisk_logisk_nw_diagram_skitse.jpg)
## Logisk netværksdiagram
![Logisk netværks diagram](images/øvelse_50/netværksdiagram.drawio.svg)
## Fysisk netværksdiagram
![fysisk netværks diagram](images/øvelse_50/fysisk_netværksdiagram.drawio.svg)