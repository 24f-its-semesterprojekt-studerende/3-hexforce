# Øvelse 101 - Trådløs sikkerhed viden

1. Hvad er et SSID?
    - Service Set Identifer, navnet på wi-fi'et.

2. Hvad er WEP, WPA, WPA2 og WPA3? Regnes de alle for sikre?
    - Wired Equivalent Privacy (WEP), bruges ikke længere. 
    - Wi-Fi Protected Access (WPA), bruger TKIP (Temporal Key Integrity Protocol), en per-packet key, erstattet af WPA2.
    - WPA2, 2004, 4-way-handshake, bruger en Pre-Shared Key (PSK). Erstattet af WPA3. Ikke sikker (Se PMKID interception).
    - WPA3, 2018, 192-bit kryptografisk strength. Bruger også PSK.

3. Hvad er forskellen på PSK og IEEE 802.1X/EAP autentificering?
    - Pre-Shared Key er en secret der er delt og brugt af alle på et netværk (Wi-Fi password).
    - Extensible Authentication Protocol er et framework for implementering, ikke en mekanisme for det. 
        - 802.1X er den standard der definer hvordan EAP skal enkapsuleres over netværk, både wired og wireless.
            - Dette er kendt som EAPOL (EAP over Lan)
            - Bruger 3 dele, en supplicant (clienten), en authenticator (access point) og en authentication server.
                - Klienten sender deres details til authenticator som tjekker dem med authentication serveren.

4. Hvad er formålet med Wi-Fi 4 way handshake? Kan det misbruges? Hvis ja, hvordan?
    - Formålet er at oprette en krypteret forbindelse mellem klient of access point. 
    - Det kan misbruges ved "Key reinstallation attacks".

5. Hvad er et PMKID interception attack? Beskriv med egne ord hvordan det fungerer
    - Brute force as password ved at intercept en del af EAPOL.

6. Hvad er en lavpraktisk måde at beskytte netværk med PSK?
    - Brug et langt kodeord.
       - Passphrase