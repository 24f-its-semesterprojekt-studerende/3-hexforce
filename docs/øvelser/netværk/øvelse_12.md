# Øvelse 12 - Protokoller og OSI model


Åben wireshark og lyt til eth0

tryk på stop efter noget tid

gem som `.pcapng` 






## Protokollernes fordeling
Tryk `Statistics`-> `Protocol Hierarchy` i Wireshark  
![alt text](images/øvelse_12/image.png){ width="500" }  
Under alle packets kan man se OSI moddellen:  
Ethernet II - Data Link lag (2)  
Internet Protocol Version 4 - Network lag (3)  
User Datagram Protocol - Transport lag (4)  
Domain Name System - Application lag (7)

![alt text](images/øvelse_12/image-1.png){ width="400" }  
Protokoller der forekommer: IPv6, UDP, DHCPv6, IPv4, UDP, QUIC IETF, DNS, TCP, TLS, HTTP, OCSP, ICMP, DNS

