# Øvelse 82 - Graylog og Firewall regler

Det er nødvendigt at tillade trafik fra opnsense på 01MGMT netværket til Grayloggen på 03MONITOR netværket. Dette for er at tillade data fra syslog og netflow der skal sendes fra opnsense til Graylog og for at få adgang til Graylogs webinterface.

Vi laver Firewall Aliaser for port `514` som er syslog, port `2250` som er netflow og port `9100` som er webinterface.  
Dette kan ses på billedet herunder:
![alt text](images/øvelse_82/image-3.png)
For at se eller ændre Aliases har vi tilgået OPNsense's webinterface. Her har vi gået ned i Firewall->Aliases og her ses de Aliases, som vi selv har lavet.  

Aliasen for "Ports from Graylog" har vi tilføjet til firewall reglerne over 01MGMT netværkssegmentet, dette kan ses herunder.  
![alt text](images/øvelse_82/image-4.png)
