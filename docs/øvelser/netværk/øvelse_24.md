# Øvelse 24 - opnsense hærdning

1. Vi bruger vores ProxMox OPNsense instans.

2. N/A

3. Det er blevet valgt at implementerer CIS pfSense Firewall Benchmark 

    4.1.1 Ensure no Allow Rule with Any in Destination Field present in the Firewall Rules (side 43).  
    4.1.2 Ensure no Allow Rule with Any in Source field present in the Firewall Rules (side 45).  
    4.1.3 Ensure no Allow Rule with Any in Services field present in the Firewall Rules (side 47).   
    4.1.4 Ensure there are no Unused Policies (side 49).  
    4.1.5 Ensure Logging is Enable for All Firewall Rules (side 50).  
    4.1.6 Ensure ICMP Request is securely configured (side 52).  

4. Implementering af benchmarks  
    Under ses skærmbilleder før og efter implementation  
    4.1.1:  
    Før vi ændre på any destination.  
    ![ø24-1](images/øvelse_24/ø24-1.png)  
    Efter vi har fulgt kontrol 4.1.1  
    ![ø24-2](images/øvelse_24/ø24-2.png)

    4.1.2: Var allerede opfyldt.

    4.1.3:  
    Før vi har ændret noget med services (vi ser at der snakkes om port her.) 
    ![ø24-3](images/øvelse_24/ø24-3.png)  
    Efter vi har ændret de protokoller og porte der er tilladt.  
    ![ø24-4](images/øvelse_24/ø24-4.png) 

    4.1.4: Opfyldt da der ikke er nogen Firewall policies vi ikke bruger.  

    4.1.5:  
    Under firewall rules > tryk edit. I den menu findes nedestående indstilling.  
    Før vi enabler logging på Firewall policy.  
    ![ø24-6](images/øvelse_24/ø24-6.png)  
    Efter logging er enabled på Firewall policy.  
    ![ø24-7](images/øvelse_24/ø24-7.png)

    4.1.6:  
    Herunder ses efter vi har sat en regel op for at tillade echo replies på Firewallen.  
    ![ø24-8](images/øvelse_24/ø24-8.png)