# Øvelse 92 - ARP spoofing med Ettercap

1. Start to virtuelle maskiner på samme netværk (f.eks vmnet8) den ene skal være Kali.

    Kali maskinen kalder vi "kali" og den anden "victim"

2. Find MAC og ip adresserne på både kali og victim

    Notér dem ned

3. Start wireshark på kali

4. Start `ping 8.8.8.8` på victim

    Kan det ses i wireshark på kali?
     - Nej det kan ikke ses på kali 
    Hvorfor Ja? hvorfor nej?
     - Packets bliver kun sendt til den enhed som den packet er beregnet til. 

5. Start ettercap

    Add Victim som target 1 og router/default gataway som target 2

    Start ARP poisoning

6. Start `ping 8.8.8.8` på victim

    Kan det ses i wireshark på kali?
     - ja
    Hvorfor Ja? hvorfor nej? Er der noget at bemærke?
     - Denne gang bliver alt trafikken fra victim sendt til MAC adressen for kali maskinen. Kali maskinen videre sender det.

7. Prøv at logge ind fra victim på adressen `http://testphp.vulnweb.com/login.php` 

    Ser du noget i ettercap? Hvis ja, hvad?

5. Giv dine bedste bud på hvordan ARP spoofing kan mitigeres, brug f.eks [MITRE attack](https://attack.mitre.org/tactics/TA0009/)
8. Opsamling på klassen - en gruppe præsenterer

