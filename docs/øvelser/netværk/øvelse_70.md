# Øvelse 70 - Dokumentation og IPAM

1.Hvad bruges en IPAM til:
Bruges til at holde styr på IP addresser

scanning af IP 

integreres med DNS DHCP

overvågning og Administration af IP addresser

Gør administration mere simpelt.

Open source IPAM løsninger: phpIPAM, netbox

3.Netbox:

- Kan:
    - IP address management
    - Oversigt over infrastruktur
        - Sites, kabler, racks, enheder, virtuelle maskiner, logging, power distribution
    - Customizability
- Kan ikke:
    - Network monitoring
    - DNS server
    - RADIUS server
    - Configuration management
    - Facilities management

4.Set netbox VM op i vmware workstation og log ind på web interfacet. Brug getting started fra netbox dokumentationen 
    (som er lavet med mkdocs!!)