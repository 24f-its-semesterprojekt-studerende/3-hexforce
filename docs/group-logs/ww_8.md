---
 hide:
#  - footer
---

# Læringslog uge 8

## Emner
Ugens emner er:

- CIS kontroller, CIS benchmarks, Mitre ATT&CK
- NMAP, wireshark, netærks segmentering, firewalls, opensense

## Mål for ugen

### Praktiske mål

**Praktiske opgaver vi har udført**

Netværk:

- [Øvelse 51 - Netværksdesign med netværkssegmentering](../øvelser/netværk/øvelse_51.md)
- [Øvelse 60 - Viden om firewalls](../øvelser/netværk/øvelse_60.md)
- Øvelse 52 - Praktisk netværkssegmentering i vmware workstation med opnsense.
- Øvelse 61 - Konfigurering af firewall i opnsense

Systemsikkerhed:

- [Øvelse 47 - CIS kontroller](../øvelser/SystemSikkerhed/øvelse_47.md)
- [Øvelse 48 - Mitre ATT&CK Taktikker, Teknikker, mitigering & detektering](../øvelser/SystemSikkerhed/øvelse_48.md)
### Læringsmål

**Læringsmål vi har arbejdet med**

- **Generelle ting:**
    - Følge benchmarks CIS benchmarks
    - Generalle sikkerheds CIS kontroller
    - Relavante it trusler
    
- **Viden:**
    - Sikkerhed i TCP/IP
    - Hvilke enheder, der anvender hvilke protokoller
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
    - Generelle governance principper / sikkerhedsprocedure
    - Relevante it-trusler
    
- **Færdigheder:**
    - Den studerende kan følge et benchmark til at sikre opsætning af enheder.
    - Identificere sårbarheder som et netværk kan have
    
- **Kompetencer:**
    - Designe, konstruere og implementere samt teste et sikkert netværk

## Reflektioner over hvad vi har lært
- System sikkerhed:
    - Vi har gennemgået og identificeret kendte angreb vha. Mitre ATT&CK database.
        - TA0004
        - T1548
        - M1026
        - DS0022
    - Vi har fået ideer til semestreprojektet gennem CIS kontroller og hvordan det måske kan bruges i den.
        - Samt måske ideer til at kunne bruge ProxMox serveren til at lære Linux bedre at kende, eller andre ting fra undervisningen.
    - Vi har arbejdet med CIS Benchmarks
        - Arbejdet Virtual Box Ubuntu, hvor vi har kigget på forskellige benchmarks ved at lave CLI i Ubuntu for aat se om de foranstaltninger allerede var installeret eller om vi skulle installere og konfigurere dem.
    
- Netværk- og Kommunikationssikkerhed:
    - Opsætning af Firewalls på OPNsense VMware.
        - Også potentielt noget der kan være relevant inden for semestre projektet.
    - Identificeret forskellige typer af Firewalls.
        - PakkeefiltreringsFirewall
        - Stateful Inspection Firewall.
        - Next-gen Firewall (NGFW)
        - mm.
    - Vi har arbejdet med segmentering af netværk for forskellige afdelinger i en virksomhed.
        - Hvordan segmenteringen af netværket skal foregå og hvilke afdelinger, som skal have adgang til hvilke netværk.
        - Vi har lavet en smule opsamling om hvor, det kan være muligt eller en god ide at opsætte firewalls og evt DMZ.

## Andet
- Lært at Virtual Box ikke er ligeså godt som VMware. 