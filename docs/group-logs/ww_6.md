---
 hide:
#  - footer
---

# Læringslog uge 6 - *Opstart uge 2*

## Emner
Ugens emner er:

- Virtuelle Maskiner
- Netværk

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål
- Forståelse af fagenes formål
- Forståelse af fagenes læringsmål
- Opsætning at Virtuelle maskiner

**Praktiske opgaver vi har udført**
- Opsætning af VMs i VMware
- Opsætning af VMs i VirtualBox
- Opsætning af VMs i Proxmox

- netværk og kommunikationssikkerhed
    - [Øvelse 2](../øvelser/netværk/øvelse_2.md)
### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
    - Hvilke enheder, der anvender hvilke protokoller
- **Kompetencer:**
    - håndtere enheder på command line-niveau


## Reflektioner over hvad vi har lært

- Skabt et grundlag for at arbejde videre med virtuelle maskiner, som vi kan bruge i kommende undervisning og projekter.