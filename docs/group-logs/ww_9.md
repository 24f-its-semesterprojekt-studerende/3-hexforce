---
 hide:
#  - footer
---

# Læringslog uge 09

## Emner
Ugens emner er:

- System Sikkerhed:  
    - Bruger systemer og rettigheder i Linux  
- Netværks-komminukations sikkerhed:  
    - Dokumentation af netværk med IPAM
    - Beskyttelse af netværksenheder med CIS18 firewall benchmarks.  
    - Designe netværk ved at træne i at designe netværk med sikkerhed    
        - Segmentering

## Mål for ugen

Herunder kan du læse om hvilke mål vi har arbejdet med i denne uge

### Praktiske mål

- Den studerende kan oprette, slette eller ændre en bruger i bash shell  
- Den studerende kan ændre bruger rettighederne i bash shell   
- Den studerende har en grundlæggende forståelse for principle of least privilege  
- Den studerende har en grundlæggende forståelse for mandatory og discretionary access control (MAC/DAC).  

**Praktiske opgaver vi har udført**


- Netværk:
    - [Øvelse 70 - IPAM med netbox](../øvelser/netværk/øvelse_70.md)  
    - [Øvelse 24 - OPNsense hærdning](../øvelser/netværk/øvelse_24.md)  
    - Øvelse 53 - Design af sikkert Netværk.  
- Systemsikkerhed:
    - Øvelse 9
    - [Øvelse 10 - Bruger kontør i Linux](../øvelser/SystemSikkerhed/øvelse_10.md)
    - [Opsætning af Ubuntu server på Proxmox](../øvelser/SystemSikkerhed/uge_09_Proxmox.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
    - OS roller ift. sikkerhedsovervejelser
- **Færdigheder:**
    - Identificere sårbarheder som et netværk kan have
    - Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:**
    - Designe, konstruere og implementere samt teste et sikkert netværk
    - Håndtere enheder på command line-niveau
    - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre it-sikkerhedsmæssige hændelser

## Reflektioner over hvad vi har lært
- Brugen af netbox til IPAM og andet management vil være en meget god ide at have med i semesterprojektet.
- Ændring af rettigheder for brugere igennem CLI commands i Ubuntu, har været spændende og interessant.
- Angivelse af rettigheds grupper og rettighed til om en bruger skal kunne Read, Write eller Eksekver en fil.

- Opsætning at Ubuntu på promox, hvor vi derefter har tilføjet flere bruger og givet dem sudo adgang har været fedt at arbejde med i  praksis
- Lås brugerkontøn for root brugeren.
    - Det er godt at få det i fingrene lige bagefter og bruge det vi lige har lært i undervisningen.
## Andet
Lært at opsætte tabeller i markdown:
|create|
|------|
|create|
|create|