---
 hide:
#  - footer
---

# Læringslog uge 5 - *Opstart*

## Emner
Ugens emner er:

- Opstart
- Opsætning

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**


- Fysisk opsætning af Dell 5820

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** håndtere enheder på command line-niveau


## Reflektioner over hvad vi har lært

- ..

## Andet