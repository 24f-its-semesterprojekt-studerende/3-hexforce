---
 hide:
#  - footer
---

# Læringslog uge xx - *Titel*

## Emner
Ugens emner er:

- OSI netværk enheder og protokoller
- Introduktion til OS
- Grundlæggene Linux commands

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål
Vi:

- har kendskab til de grundlæggende Linux kommandør.
- kan navigere i Linux file struktur.
- kan oprette og slette filer i Linux
- kan oprette og slette directories i Linux
- kan fortag en søg på en file eller mappe i Linux
- kan identificer en proces
- kan "dræbe en proces"

**Praktiske opgaver vi har udført**

Netværk:

- [Øvelse 12 - Protokolforståelse](../øvelser/netværk/øvelse_12.md)
- [Øvelse 13 - Wireshark og OSI](../øvelser/netværk/øvelse_13.md)
- [Øvelse 23 - OPNsense viden](../øvelser/netværk/øvelse_23.md)
- [Øvelse 50 - Tegn netværksdiagram](../øvelser/netværk/øvelse_50.md)

Systemsikkerhed:

- Basic Linux commands
- Linux file struktur
- Søgning i Linux file system

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**

    - Hvilke enheder, der anvender hvilke protokoller
    - Adressering i de forskellige lag
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).

    - relevante it trusler
    - relevante sikkerhedsprincipper
- **Færdigheder:** 
- **Kompetencer:** 
    - håndterer enheder på command line niveau

## Reflektioner over hvad vi har lært

- Kan se teori i praksis, giver en bedre forståelse
    - Bruge Wireshark til sniffing og se: 
        - TCP handshake
        - OSI model
        - Sammenligne med standarder (RFC9293)
    - Linux commands, så man får det i fingrene.

## Andet